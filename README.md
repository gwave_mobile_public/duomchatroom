# DuomChatRoom

[![CI Status](https://img.shields.io/travis/kuroky/DuomChatRoom.svg?style=flat)](https://travis-ci.org/kuroky/DuomChatRoom)
[![Version](https://img.shields.io/cocoapods/v/DuomChatRoom.svg?style=flat)](https://cocoapods.org/pods/DuomChatRoom)
[![License](https://img.shields.io/cocoapods/l/DuomChatRoom.svg?style=flat)](https://cocoapods.org/pods/DuomChatRoom)
[![Platform](https://img.shields.io/cocoapods/p/DuomChatRoom.svg?style=flat)](https://cocoapods.org/pods/DuomChatRoom)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DuomChatRoom is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'DuomChatRoom'
```

## Author

kuroky, kai.he@duom.com

## License

DuomChatRoom is available under the MIT license. See the LICENSE file for more info.
