#
# Be sure to run `pod lib lint DuomChatRoom.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DuomChatRoom'
  s.version          = '1.0.0'
  s.summary          = 'Duom语聊房'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/gwave_mobile_public/duomchatroom'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'HeKai_Duom' => 'kai.he@duom.com' }
  s.source           = { :git => 'https://gitlab.com/gwave_mobile_public/duomchatroom.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '12.0'
  s.swift_version    = '5.0'

  s.source_files = 'DuomChatRoom/Classes/**/*'
  s.resources    = 'DuomChatRoom/Assets/*', 'DuomChatRoom/LottieSources/*'
 
   
  s.dependency 'DuomBase'
  s.dependency 'AgoraRtcEngine_iOS', '4.0.0-rc.1'
  s.dependency 'KKIMModule'
  s.dependency 'DuomNetworkKit'
  
  
end
