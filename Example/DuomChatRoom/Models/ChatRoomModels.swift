//
//  ChatRoomModels.swift
//  DuomChatRoom_Example
//
//  Created by kuroky on 2022/10/9.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import DuomNetworkKit

enum MemberRole: String, JSONCodableEnum {
    case speaker = "SPEAKER"
    case manager = "MANAGER"
}

enum MicStatus: String, JSONCodableEnum {
    case mute = "CONNECT"
    case unMute = "DISCONNECT"
}

struct RoomMemberModel: JSONCodable {
    var customerId: String?
    var micStatus: MicStatus = .mute
    var role: MemberRole = .speaker
    var groupMute: Bool = false
}

// 预览页面列表
struct PreDetailModel: JSONCodable {
    var audienceCount: Int?
    var micMembers: [RoomMemberModel] = []
}


