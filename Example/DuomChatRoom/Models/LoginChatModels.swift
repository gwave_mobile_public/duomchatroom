//
//  LoginChatModels.swift
//  DuomChatRoom_Example
//
//  Created by kuroky on 2022/10/8.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import DuomNetworkKit

struct ChatSignModel: JSONCodable {
    var imUid: String?
    var sign: String?
}

