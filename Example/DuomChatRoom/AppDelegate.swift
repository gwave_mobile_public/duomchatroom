//
//  AppDelegate.swift
//  DuomChatRoom
//
//  Created by kuroky on 09/19/2022.
//  Copyright (c) 2022 kuroky. All rights reserved.
//

import UIKit
import KKIMModule
import DuomBase
import DuomNetworkKit
import RxCocoa
import RxSwift

let currentEventId = "2022102408493359500217"

// Token deviceID
//     listener
let currentUserID = "2022101203532335902002"
let device_id = "00008110-00184CC234A1401E"
let JWT_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkIjoxNjY2MjUzMjE2NDYwLCJkZXZpY2VJZCI6IjAwMDAwMDAwLTAwMDAtMDAwMC0wMDAwLTAwMDAwMDAwMDAwMCIsImlwIjoiOC4yMTAuMTUwLjMiLCJsb2NhbGUiOiJlbiIsImxvZ2luVHlwZSI6IjEiLCJwdXNoVG9rZW4iOiJ0b2tlbiIsInB1c2hUb2tlblR5cGUiOiJpb3MiLCJ0aW1lIjoxNjY2ODU4MDE2NDYwLCJ1c2VyQWdlbnQiOiJEdW9tLzIwMjIxMDE5MTcyMCBDRk5ldHdvcmsvMTMyNy4wLjQgRGFyd2luLzIxLjIuMCIsInVzZXJfaWQiOiIyMDIyMTAxMjAzNTMyMzM1OTAyMDAyIn0.GzwdEhpfoJkLEbd5PDQe9hA3oaTUHMmya4VuVmSSV1s"


/*
 admin:
 */
//    let device_id = "duom_3124F13E-639B-4D5E-86E8-6AE5D2FE4012"
//let currentUserID = "2022101408452496602007"
//    let JWT_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkIjoxNjY1ODI0NjY2NjU4LCJkZXZpY2VJZCI6IjAwMDAwMDAwLTAwMDAtMDAwMC0wMDAwLTEyMyIsImlwIjoiOC4yMTAuMTUwLjMiLCJsb2NhbGUiOiJlbl9VUyIsImxvZ2luVHlwZSI6IjAiLCJwdXNoVG9rZW4iOiI2NzczNjkzOC00MDEwLTU0MjQtNjg4OS0xNTMyNjU1MTIwMDAiLCJwdXNoVG9rZW5UeXBlIjoiaW9zIiwidGltZSI6MTY2NjQyOTQ2NjY1OCwidXNlckFnZW50IjoiUG9zdG1hblJ1bnRpbWUvNy4yOS4yIiwidXNlcl9pZCI6IjIwMjIxMDE0MDg0NTI0OTY2MDIwMDcifQ.96UZCnr2S6fV9V8eKUJwfE_BhEqKm2_3unnCEQdfqag"



// Owner:
//    let device_id = "duom_3124F13E-639B-4D5E-86E8-6AE5D2FE401F"
//    let currentUserID = "2022101407082545802006"
//    let JWT_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkIjoxNjY2MTYyODE1ODg0LCJkZXZpY2VJZCI6IjAwMDAwMDAwLTAwMDAtMDAwMC0wMDAwLTAwMDAwMDAwMDAwMCIsImlwIjoiNDcuMjQzLjEzNC42NSIsImxvY2FsZSI6InpoIiwibG9naW5UeXBlIjoiMSIsInB1c2hUb2tlbiI6InRva2VuIiwicHVzaFRva2VuVHlwZSI6ImlvcyIsInRpbWUiOjE2NjY3Njc2MTU4ODQsInVzZXJBZ2VudCI6IkR1b20vMjAyMjEwMTkxMTAwIENGTmV0d29yay8xMzMzLjAuNCBEYXJ3aW4vMjEuNS4wIiwidXNlcl9pZCI6IjIwMjIxMDE0MDcwODI1NDU4MDIwMDYifQ.mggTqvYdu-d584xoBLKN_yg-yfik7_81U4eCuaEni3A"


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    // Agora
    let AgoraAppId: String = "7c939bd01ad54984978ad725c58e2db1"
    
    private let disposeBag = DisposeBag()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = .white
        window?.makeKeyAndVisible()
        let nav = UINavigationController(rootViewController: ViewController())
        window?.rootViewController = nav
        
        initIMSDK()
        initLogin()
        
        return true
    }
    
    // 获取签名--> 登陆IM--> 绑定Im用户
    private func initLogin()  {
        // NetWork
        NetworkConfig.setupDefault(header: ["device_id": device_id,
                                            "JWT_TOKEN": JWT_TOKEN],
        plugins: [NetworkDebuggingPlugin(), NetworkAnalysisPlugins()])
        
        loginChatProvider.rx
            .request(.getIMSign)
            .map(ChatSignModel.self)
            .showHUD()
            .subscribe(onSuccess: { [weak self] model in
                guard let imUid = model.imUid, let sign = model.sign, let self = self else { return }
                self.loginIm(imUid, sign: sign)
            }, onFailure: { error in
                print("\(error)")
            })
            .disposed(by: disposeBag)
    }
    
    // init im
    private func initIMSDK()  {
        DuomHelper.sharedInstance().initIMSDK(2000097)
    }
    
    // login im
    private func loginIm(_ uid: String, sign: String)  {
        DuomHelper.sharedInstance().loginIMSDK(uid, userSig: sign) { [weak self] in
            guard let self = self else { return }
            Logger.log(message: "IM SDK Login Success", level: .info)
            loginChatProvider.rx
                .request(.bindIM(imUid: uid))
                .mapJSON()
                .showHUD()
                .subscribe(onSuccess: { json in
                    print("\(json)")
                })
                .disposed(by: self.disposeBag)
        } fail: { code, desc in
            Logger.log(message: "IM SDK Login Failure:\(code)------------\(desc)", level: .error)
        }
    }
    
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

