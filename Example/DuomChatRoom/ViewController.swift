//
//  ViewController.swift
//  DuomChatRoom
//
//  Created by kuroky on 09/19/2022.
//  Copyright (c) 2022 kuroky. All rights reserved.
//

import UIKit
import DuomChatRoom
import RxCocoa
import RxSwift

class ViewController: UIViewController {

    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.view.backgroundColor = .black
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        let vc = ListenerEnterRoomViewController(role: .listener).then {
//            $0.viewModel = ChatRoomViewModel(role: .listener)
//        }
//        self.navigationController?.present(vc, animated: true)
        
        let vc = ChatRoomMainViewController().then {
            $0.viewModel = ChatRoomViewModel(role: currentUserID == "2022101407082545802006" ? .owner : .listener, eventId: currentEventId, userId: currentUserID, roomName: "test Name", roomTopic: "test topic")
        }
        self.navigationController?.present(vc, animated: true)
        
    }
    
}

