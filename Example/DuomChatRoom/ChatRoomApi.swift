////
////  ChatRoomApi.swift
////  DuomChatRoom_Example
////
////  Created by kuroky on 2022/9/29.
////  Copyright © 2022 CocoaPods. All rights reserved.
////
//
//import Foundation
//import DuomNetworkKit
//import Moya
//
//let eventID = "2022100811424118700020"
//
//let chatRoomProvider = MoyaProvider<ChatRoomApi>(plugins: NetworkConfig.plugins)
//
//enum ChatRoomApi {
//    case creatRoom
//    case preDetailList(cid: String)
//    case leaveRoom
//}
//
//extension ChatRoomApi: NetworkAPI {
//    var parameters: [String : Any] {
//        switch self {
//        case .creatRoom:
//            return [:]
//        case let .preDetailList(cid: cid):
//            return ["cid": cid]
//        case .leaveRoom:
//            return [:]
//        }
//    }
//
//    var path: String {
//        switch self {
//        case .creatRoom:
//            return ""
//        case .preDetailList:
//            return "/v1/celebrity/event/mic/detail/list"
//        case .leaveRoom:
//            return "/v1/celebrity/event/mic/detail/list"
//        }
//    }
//    
//    var method: Moya.Method {
//        switch self {
//        case .creatRoom, .leaveRoom:
//            return .post
//        case .preDetailList:
//            return .get
//        }
//    }
//
//    var task: Task {
//        switch self {
//        case let .preDetailList(cid: cid):
//            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
//        default:
//            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
//        }
//    }
//
//}
