//
//  BaseViewController.swift
//  Duom
//
//  Created by kuroky on 2022/8/23.
//


import UIKit

public extension UIView {
    func setupShadow(opacity: Float = 0, radius: CGFloat = 0, offset: CGSize = .zero, color: UIColor = .black) {
        layer.shadowOpacity = opacity
        layer.shadowRadius = radius
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
    }

    convenience init(backgroundColor: UIColor = .clear) {
        self.init(frame: .zero)
        self.backgroundColor = backgroundColor
    }

    func setupCorners(rect: CGRect, corners: UIRectCorner, radii: CGSize, maskToBounds: Bool = true) {
        let bezier = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: radii)
        let shapeLayer = CAShapeLayer()
        shapeLayer.frame = rect
        shapeLayer.path = bezier.cgPath
        layer.mask = shapeLayer
        layer.masksToBounds = maskToBounds
    }

    func roundCorners(radius: CGFloat) {
        layer.cornerRadius = radius
        layer.masksToBounds = true
    }
    
    func rounded(color: UIColor? = nil, borderWidth: CGFloat = 0, radius: CGFloat? = nil) {
        self.layer.cornerRadius = radius ?? min(self.bounds.width, self.bounds.height) / 2
        if let borderColor = color {
            self.layer.borderColor = borderColor.cgColor
        }
        self.layer.borderWidth = borderWidth
    }

    var x: CGFloat {
        get { return self.frame.origin.x }
        set(x) {
            var frame = self.frame
            frame.origin.x = x
            self.frame = frame
        }
    }

    var y: CGFloat {
        get { return self.frame.origin.y }
        set(y) {
            var frame = self.frame
            frame.origin.y = y
            self.frame = frame
        }
    }

    var w: CGFloat {
        get { return self.frame.size.width }
        set(w) {
            var frame = self.frame
            frame.size.width = w
            self.frame = frame
        }
    }

    var h: CGFloat {
        get { return self.frame.size.height }
        set(h) {
            var frame = self.frame
            frame.size.height = h
            self.frame = frame
        }
    }

    @discardableResult
    func asSubViewInsertView(view: UIView, at index: Int? = nil) -> Self {
        if let viewIndex = index {
            view.insertSubview(self, at: viewIndex)
        } else {
            view.addSubview(self)
        }
        return self
    }
}

public extension UIScrollView {
    func scrollToBottom(animated: Bool = true) {
        var off = self.contentOffset
        off.y = self.contentSize.height - self.bounds.size.height + self.contentInset.bottom
        self.setContentOffset(off, animated: animated)
    }
}
