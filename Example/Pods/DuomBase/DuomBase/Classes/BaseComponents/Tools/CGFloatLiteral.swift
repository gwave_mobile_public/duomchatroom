//
//  BaseViewController.swift
//  Duom
//
//  Created by kuroky on 2022/8/23.
//


import CoreGraphics
import UIKit


/// Ceil to snap pixel
public func snap(_ x: CGFloat) -> CGFloat {
    let scale = UIScreen.main.scale
    return ceil(x * scale) / scale
}

public func snap(_ point: CGPoint) -> CGPoint {
    return CGPoint(x: snap(point.x), y: snap(point.y))
}

public func snap(_ size: CGSize) -> CGSize {
    return CGSize(width: snap(size.width), height: snap(size.height))
}

public func snap(_ rect: CGRect) -> CGRect {
    return CGRect(origin: snap(rect.origin), size: snap(rect.size))
}


public extension CGSize {
    var DScale: CGSize {
        return .init(width: width.DScale, height: height.DScale)
    }

    var toCeil: CGSize {
        return .init(width: width.toCeil, height: height.toCeil)
    }
}

public extension CGFloat {
    var toCeil: CGFloat {
        return Darwin.ceil(self)
    }

    var DScale: CGFloat {
        return snap(self * UIScreen.duomAspectScale)
    }
}

public extension Float {
    var f: CGFloat {
        return CGFloat(self)
    }

    var toCeil: CGFloat {
        return Darwin.ceil(f)
    }

    var DScale: CGFloat {
        return snap(f * UIScreen.duomAspectScale)
    }
}

public extension IntegerLiteralType {
    var f: CGFloat {
        return CGFloat(self)
    }

    var toCeil: CGFloat {
        return Darwin.ceil(f)
    }

    var DScale: CGFloat {
        return snap(f * UIScreen.duomAspectScale)
    }
}

public extension FloatLiteralType {
    var f: CGFloat {
        return CGFloat(self)
    }

    var toCeil: CGFloat {
        return Darwin.ceil(f)
    }

    var DScale: CGFloat {
        return snap(f * UIScreen.duomAspectScale)
    }
}

public extension CGFloat {
    public static var screenW: CGFloat { return UIScreen.main.bounds.size.width }

    public static var screenH: CGFloat { return UIScreen.main.bounds.size.height }
}
