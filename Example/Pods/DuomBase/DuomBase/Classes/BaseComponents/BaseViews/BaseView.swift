//
//  BaseViewController.swift
//  Duom
//
//  Created by kuroky on 2022/8/19.
//

import UIKit

open class BaseView: UIView {
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        addActions()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }

    open func setupUI() {}
    
    open func addActions() {}
}
