//
//  BaseViewController.swift
//  Duom
//
//  Created by kuroky on 2022/8/19.
//

import UIKit

open class BaseTableViewCell: UITableViewCell {
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        addUIActions()
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.selectionStyle = .none
    }

    override open func awakeFromNib() {
        super.awakeFromNib()
    }
    
    open func addUIActions() {
        
    }

    override open func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
