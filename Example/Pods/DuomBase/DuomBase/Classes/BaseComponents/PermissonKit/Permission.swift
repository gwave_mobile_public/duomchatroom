//
//  Permission.swift
//  Duom
//
//  Created by HK on 2019/6/14.
//  Copyright © 2019 butcheryl. All rights reserved.
//

import Foundation

public enum Permission {
    case location(LocationUsage)
    case notifications(NotificationOptions)
    case camera
    case photos
}

public extension Permission {
    typealias Callback = (Permission.Status) -> Void
}

public extension Permission {
    var proposer: Proposer {
        switch self {
        case .location(let usage):
            return LocationProposer(locationUsage: usage)
        case .notifications(let options):
            return NotificationsProposer(options: options)
        case .camera:
            return CameraProposer()
        case .photos:
            return PhotosProposer()
        }
    }
    
    func authorization(_ callback: (Permission.Status, Proposer) -> Void) {
        let proposer = self.proposer
        callback(proposer.currentStatus, proposer)
    }
}

public extension Permission {
    func proposeToAccess(pre prePermissionAction: ((@escaping (Bool) -> Void) -> Void)? = nil, agreed successAction: @escaping (Permission.Status) -> Void, rejected failureAction: @escaping (Permission.Status) -> Void) {
        authorization { status, proposer in
            switch status {
            case .authorized, .whenInUse:
                successAction(status)
            case .denied, .disabled:
                failureAction(status)
            case .notDetermined:
                let action = prePermissionAction ?? { $0(true) }
                
                action { needPropose in
                    if needPropose {
                        proposer.proposeToAccess { status in
                            if status == .authorized || status == .whenInUse {
                                successAction(status)
                            } else {
                                failureAction(status)
                            }
                        }
                    } else {
                        failureAction(status)
                    }
                }
            }
        }
    }
}

public extension Permission {
    enum Status: String, CustomStringConvertible, CustomDebugStringConvertible {
        case denied = "Denied"
        case disabled = "Disabled"
        case whenInUse = "WhenInUse"
        case authorized = "Authorized"
        case notDetermined = "Not Determined"
        
        internal init?(string: String?) {
            guard let string = string else { return nil }
            self.init(rawValue: string)
        }
        
        public var description: String {
            return rawValue
        }
        
        public var debugDescription: String {
            return "debug: \(description)"
        }
    }
}

public extension Permission {
    enum LocationUsage {
        case always
        case whenInUse
    }
    
    struct NotificationOptions: OptionSet {
        public var rawValue: Int
        
        public init(rawValue: Int) {
            self.rawValue = rawValue
        }
        
        public static let badge = NotificationOptions(rawValue: 1 << 0)
        
        public static let sound = NotificationOptions(rawValue: 1 << 1)
        
        public static let alert = NotificationOptions(rawValue: 1 << 2)
        
        @available(iOS 10.0, *)
        public static let carPlay = NotificationOptions(rawValue: 1 << 3)
        
        @available(iOS 12.0, *)
        public static let criticalAlert = NotificationOptions(rawValue: 1 << 4)
        
        @available(iOS 12.0, *)
        public static let providesAppNotificationSettings = NotificationOptions(rawValue: 1 << 5)
        
        @available(iOS 12.0, *)
        public static let provisional = NotificationOptions(rawValue: 1 << 6)
    }
}

extension Permission: CustomStringConvertible {
    public var description: String {
        switch self {
        case .location:
            return "Location"
        case .notifications:
            return "Notifications"
        case .camera:
            return "Camera"
        case .photos:
            return "Photos"
        }
    }
}

extension Permission: CustomDebugStringConvertible {
    public var debugDescription: String {
        return "debug: \(description)"
    }
}
