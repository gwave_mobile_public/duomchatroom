//
//  Location.swift
//  Duom
//
//  Created by HK on 2019/6/14.
//  Copyright © 2019 butcheryl. All rights reserved.
//

import CoreLocation
import UIKit

class LocationProposer: NSObject, Proposer {
    var usage: Permission.LocationUsage
    
    var callback: Permission.Callback?
    
    var locationManager: CLLocationManager?
    
    var strongReference: Any?
    
    var currentStatus: Permission.Status {
        guard CLLocationManager.locationServicesEnabled() else { return .disabled }
        
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways: return .authorized
        case .authorizedWhenInUse: return .whenInUse
        case .notDetermined: return .notDetermined
        case .restricted, .denied: return .denied
        default: return .disabled
        }
    }
    
    init(locationUsage: Permission.LocationUsage) {
        usage = locationUsage
        super.init()
    }
    
    func proposeToAccess(_ callback: @escaping Permission.Callback) {
        /*
         在 iOS8 - iOS10
         NSLocationWhenInUseUsageDescription 表示应用在前台的时候可以收到更新的位置信息。
         NSLocationAlwaysUsageDescription 申请Always权限，以便应用在前台和后台都可以获取到更新的位置数据。
         
         在 iOS 11
         NSLocationWhenInUseUsageDescription 表示应用在前台的时候可以收到更新的位置信息。
         NSLocationWhenInUseUsageDescription + NSLocationAlwaysAndWhenInUseUsageDescription 申请Always权限，以便应用在前台和后台都可以获取到更新的位置数据。
         */
        if usage == .always {
            if Foundation.Bundle.main.object(forInfoDictionaryKey: .locationAlwaysUsageDescription) == nil {
                // iOS8 - iOS10
                print("WARNING: \(String.locationAlwaysUsageDescription) not found in Info.plist")
                return
            }
            
            if Foundation.Bundle.main.object(forInfoDictionaryKey: .locationAlwaysAndWhenInUseDescription) == nil {
                // iOS11
                print("WARNING: \(String.locationAlwaysAndWhenInUseDescription) not found in Info.plist")
                return
            }
        }
        
        if Foundation.Bundle.main.object(forInfoDictionaryKey: .locationWhenInUseUsageDescription) == nil {
            print("WARNING: \(String.locationWhenInUseUsageDescription) not found in Info.plist")
            return
        }
        
        self.callback = callback
        strongReference = self
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        
        if CLLocationManager.locationServicesEnabled() {
            switch usage {
            case .whenInUse:
                locationManager?.requestWhenInUseAuthorization()
            case .always:
                locationManager?.requestAlwaysAuthorization()
            }
        } else {
            callback(.disabled)
        }
    }
}

extension LocationProposer: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        DispatchQueue.main.async {
            switch status {
            case .authorizedWhenInUse:
                self.callback?(.whenInUse)
                self.strongReference = nil
            case .authorizedAlways:
                self.callback?(.authorized)
                self.strongReference = nil
            case .denied, .restricted:
                self.callback?(.denied)
                self.strongReference = nil
            case .notDetermined:
                return
            default:
                return
            }
        }
    }
}
