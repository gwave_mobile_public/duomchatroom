//
//  DuomAlertViewController.swift
//  DuomBase
//
//  Created by kuroky on 2022/9/15.
//

import UIKit

public struct DuomAlertButtonItem {
    public enum Style {
        case cancel
        case `default`
    }
    
    public var title: String
    public var style: Style
    public var dismissOnTap: Bool
    public var font: UIFont
    public var titleColor: UIColor
    public var backgroundColor: UIColor
    
    public init(title: String, style: DuomAlertButtonItem.Style, dismissOnTap: Bool, font: UIFont, titleColor: UIColor, backgroundColor: UIColor) {
        self.title = title
        self.style = style
        self.dismissOnTap = dismissOnTap
        self.font = font
        self.titleColor = titleColor
        self.backgroundColor = backgroundColor
    }
    
    public static func cancel(title: String = "CANCEL") -> DuomAlertButtonItem {
        return .init(title: title, style: .cancel, dismissOnTap: true, font: .appFont(ofSize: 14, fontType: .Inter_Blod), titleColor: .black, backgroundColor: UIColor.themeColor)
    }
    
    public static func `default`(title: String = "OK") -> DuomAlertButtonItem {
        return .init(title: title, style: .default, dismissOnTap: true, font: .appFont(ofSize: 14, fontType: .Inter_Blod), titleColor: .white, backgroundColor: UIColor.themeBlackColor)
    }
}


open class DuomAlertViewController: UIViewController, UIGestureRecognizerDelegate {
    public var buttons: [UIButton] = []
    
    public var buttonItems: [DuomAlertButtonItem] = []
    
    public var buttonContainer = UIView(backgroundColor: .clear)
    
    internal var presentationManager: DuomAlertPresentationManager
    
    private lazy var tapGesture = UITapGestureRecognizer(target: self, action: #selector(containerViewDidTap(_:))).then {
        $0.delegate = self
    }
    
    @available(*, unavailable)
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc public init(transitionStyle: DuomAlertTransitionStyle = .fadeIn) {
        presentationManager = DuomAlertPresentationManager(transitionStyle: transitionStyle)
        
        super.init(nibName: nil, bundle: nil)
        
        transitioningDelegate = presentationManager
        
        modalPresentationStyle = .custom
        
        modalPresentationCapturesStatusBarAppearance = true
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let containerView = presentationController?.containerView {
            containerView.removeGestureRecognizer(tapGesture)
            containerView.addGestureRecognizer(tapGesture)
        }
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.themeBlackColor
        view.roundCorners(radius: 5)
        
        guard buttonItems.count > 0 else { return }
        
        view.addSubview(buttonContainer)
        
        buttonContainer.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.trailing.equalTo(-16)
            make.bottom.equalTo(-24)
            make.height.equalTo(36)
        }
        
        buttons = buttonItems.enumerated().map { index, item in
            UIButton(type: .custom).then {
                $0.tag = index
                $0.titleLabel?.font = item.font
                $0.setTitleColor(item.titleColor, for: .normal)
                $0.setTitleColor(item.titleColor.withAlphaComponent(0.5), for: .highlighted)
                $0.backgroundColor = item.backgroundColor
                $0.setTitle(item.title, for: .normal)
                if item.style == .default {
                    $0.rounded(color: UIColor.textWhiteColor, borderWidth: 1)
                } 
                $0.addTarget(self, action: #selector(buttonOnTap(_:)), for: UIControl.Event.touchUpInside)
            }
        }
    }
    
    open func buttonsHorizontalLayout() {
        buttonContainer.subviews.forEach { $0.removeFromSuperview() }
        
        var lastView: UIView?

        for button in buttons {
            buttonContainer.addSubview(button)

            button.snp.makeConstraints { make in
                make.top.equalTo(0)
                make.bottom.equalTo(0)
                if let lastView = lastView {
                    make.width.equalTo(lastView.snp.width)
                    make.leading.equalTo(lastView.snp.trailing).offset(6)
                } else {
                    make.leading.equalTo(0)
                }
            }
            button.layoutIfNeeded()
            button.roundCorners(radius: min(button.bounds.height, button.bounds.width) / 2)
            lastView = button
        }

        if let lastView = lastView {
            lastView.snp.makeConstraints { make in
                make.trailing.equalTo(0)
            }
        }
    }
    
    open func buttonsVerticalLayout() {}
    
    @objc open func buttonOnTap(_ sender: UIButton) {
        if buttonItems[sender.tag].dismissOnTap {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @objc open func containerViewDidTap(_ sender: UITapGestureRecognizer) {}
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }

}
