//
//  BaseViewController.swift
//  Duom
//
//  Created by kuroky on 2022/8/23.
//

import Foundation
import UIKit

public class RoundImageButton: UIButton {
    public var color: UIColor?
    public var borderWidth: CGFloat = 1
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        rounded(color: color, borderWidth: borderWidth)
    }
}
