//
//  UIFont+Compatible.swift
//  DuomBase
//
//  Created by kuroky on 2022/9/2.
//

import Foundation
import UIKit

public enum FontType: String {
    case GT = "GT Flexa Trial"
    case Inter_Blod = "Inter-Bold"
    case Inter_Medium = "Inter-Medium"
}

public extension UIFont {
    static var fontSizeMultiplier: CGFloat = min(1.5, max(1.f, UIScreen.screenWidth / 375.f))
    
    static func appFont(ofSize size: CGFloat, fontType: FontType = .Inter_Medium) -> UIFont {
        if let font = UIFont(name: fontType.rawValue, size: size) {
            return font
        } else {
            return .systemFont(ofSize: size)
        }
    }
    
    static func appFont(ofScaleBase size: CGFloat, fontType: FontType = .Inter_Medium) -> UIFont {
        let font = UIFont.appFont(ofSize: size, fontType: fontType)
        return UIFont(descriptor: font.fontDescriptor, size: ceil(font.pointSize * fontSizeMultiplier))
    }
}

