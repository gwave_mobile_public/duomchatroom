//
//  ViewType.swift
//  Duom
//
//  Created by kuroky on 2022/8/23.
//


import RxSwift

/// 事件
public protocol ResponderChainEvent {}

/// 发出事件方实现
public protocol ResponderChainEmiter {}

/// 接收事件方实现
public typealias ResponderChainRouter = ResponderChainWrapper & ResponderChainStorage

public protocol ResponderChainWrapper {
    associatedtype Event: ResponderChainEvent

    var events: Observable<Event> { get }
}

public protocol ResponderChainStorage {}
