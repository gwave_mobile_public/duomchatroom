//
//  ViewModel.swift
//  Duom
//
//  Created by kuroky on 2022/8/19.
//

import RxSwift
import Then

public protocol Output {
    init()
}

public protocol ViewModel: AnyObject, AssociatedObjectStore, Then {
    associatedtype State: Output
    associatedtype Action
    
    var state: State { get }
    
    var action: ActionSubject<Action> { get }
    
    func transform(action: Observable<Action>) -> Observable<Action>
    
    func mutate(action: Action, state: State)
}

private var actionKey = "action"
private var stateKey = "state"
private var disposeBagKey = "disposeBag"

public extension ViewModel {
    func transform(action: Observable<Action>) -> Observable<Action> { return action }
    
    func mutate(action: Action, state: State) {}
}

extension ViewModel {
    fileprivate var disposeBag: DisposeBag { return associatedObject(forKey: &disposeBagKey, default: DisposeBag()) }
    
    public var action: ActionSubject<Action> {
        _ = _state
        return _action
    }
    
    private var _action: ActionSubject<Action> {
        return associatedObject(forKey: &actionKey, default: .init())
    }
    
    public var state: State {
        return _state
    }
    
    private var _state: State {
        if let object: State = self.associatedObject(forKey: &stateKey) {
            return object
        } else {
            let object = State()
            setAssociatedObject(object, forKey: &stateKey)
            transform(action: _action.asObservable())
                .subscribe(onNext: { [weak self] action in
                    guard let `self` = self else { return }
                    self.mutate(action: action, state: self.state)
                })
                .disposed(by: disposeBag)
            return object
        }
    }
}
