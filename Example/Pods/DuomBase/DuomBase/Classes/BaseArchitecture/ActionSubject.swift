//
//  ActionSubject.swift
//  Duom
//
//  Created by kuroky on 2022/8/19.
//

import class Foundation.NSLock.NSRecursiveLock
import RxSwift


public final class ActionSubject<Element>: ObservableType, ObserverType, SubjectType {
    public typealias E = Element
    typealias Key = UInt

    var lock = NSRecursiveLock()

    var nextKey: Key = 0
    var observers: [Key: (Event<Element>) -> ()] = [:]

    #if TRACE_RESOURCES
    init() {
        _ = Resources.incrementTotal()
    }
    #endif

    #if TRACE_RESOURCES
    deinit {
        _ = Resources.decrementTotal()
    }
    #endif

    public func subscribe<O: ObserverType>(_ observer: O) -> Disposable where O.Element == Element {
        self.lock.lock()
        let key = self.nextKey
        self.nextKey += 1
        self.observers[key] = observer.on
        self.lock.unlock()

        return Disposables.create { [weak self] in
            self?.lock.lock()
            self?.observers.removeValue(forKey: key)
            self?.lock.unlock()
        }
    }

    public func on(_ event: Event<Element>) {
        self.lock.lock()
        if case .next = event {
            self.observers.values.forEach { $0(event) }
        }
        self.lock.unlock()
    }
}
