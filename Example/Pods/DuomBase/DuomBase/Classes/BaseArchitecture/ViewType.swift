//
//  ViewType.swift
//  Duom
//
//  Created by kuroky on 2022/8/19.
//

import RxSwift
  
public protocol ViewType: AnyObject, AssociatedObjectStore {
    associatedtype TargetViewModel: ViewModel
    
    var disposeBag: DisposeBag { get set }
    
    var viewModel: TargetViewModel? { get set }
    
    func bind(viewModel: TargetViewModel)
}

var viewModelKey = "viewModel"
var isViewModelKeyBindedKey = "isViewModelKeyBindedKey"

extension ViewType {
   public var viewModel: TargetViewModel? {
        get { return self.associatedObject(forKey: &viewModelKey) }
        set {
            self.setAssociatedObject(newValue, forKey: &viewModelKey)
            self.disposeBag = DisposeBag()
            if let viewModel = newValue {
                self.bind(viewModel: viewModel)
            }
        }
    }
}
