//
//  ChatRoomRoomModel.swift
//  DuomChatRoom
//
//  Created by kuroky on 2022/12/27.
//

import Foundation
import HandyJSON
import DuomNetworkKit

// MARK: joinRoom
struct ChatRoomJoinSuccessModel: JSONCodable {
    var eventId: String?
    var groupId: String?
    var token: String?
}


// MARK: roomInfo
struct ChatRoomInfo: JSONCodable {
    var celebrity: ChatRoomCelebrityModel?
    var estimatedStart: String?
    var owner: String?
    var role: String?
    var surface: String?
    var created: String?
    var estimatedEnd: String?
    var timeZone: String?
    var description: String?
    var subscribeStatus: String?
    var title: String?
    var type: String?
    var topic: String?
    var id: String?
    var followerCount: String?
    var status: String?
    var followers: [ChatRoomFollwerModel] = [ChatRoomFollwerModel]()
}

struct ChatRoomFollwerModel: JSONCodable {
    var customerId: String?
    var avatar: String?
}

struct ChatRoomCelebrityModel: JSONCodable {
    var avatar: String?
    var customerId: String?
    var gender: String?
    var intro: String?
    var nickName: String?
}



public struct ChatRoomInfoModel: JSONCodable {
    var groupId: String?
    var event: ChatRoomInfoDetailModel?
    var token: String?
    
    public init() {}
}

public struct ChatRoomInfoDetailModel: JSONCodable {
    var estimatedStart: String?
    var owner: String?
    var surface: String?
    var created: String?
    var estimatedEnd: String?
    var topic: String?
    var timeZone: String?
    var description: String?
    var id: String?
    var title: String?
    var type: String?
    var status: String?
    
    public init() {}
}
