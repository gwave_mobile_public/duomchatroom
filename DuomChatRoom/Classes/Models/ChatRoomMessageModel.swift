//
//  ChatRoomMessageModel.swift
//  DuomChatRoom
//
//  Created by kuroky on 2022/12/27.
//

import Foundation
import HandyJSON
import DuomNetworkKit

public enum ChatMessageType: String {
    case ALL_MUTE = "1000"
    case ALL_UNMUTE = "1001"
    case MUTE = "1002"
    case UNMUTE = "1003"
    case KIKI_USER = "1004"
    case PICK_USER = "1005"
    case LEAVE_SEAT = "1006"
    case SET_MANGER = "1007"
    case CANCEL_MANAGER = "1008"
    case CHANGE_GROUP_NAME = "1009"
    case DESTROY_GROUP = "1010"
    case LEVE_GROUP = "1011"
    case REQUEST_TO_SPEAKER = "0009"
}

public struct ChatRoomMessageModel: JSONCodable {
    var msg: ChatRoomMsgModel?
    var customMsgType: String?
    var businessID: String?
    
    public init() {}
    
    public init(msg: ChatRoomMsgModel, customMsgType: String? = nil, businessID: String? = nil) {
        self.msg = msg
        self.customMsgType = customMsgType
        self.businessID = businessID
    }
}

public struct ChatRoomMsgModel: JSONCodable {
    var customerId: String?
    var cmd: String?
    var token: String?
    
    public init() {}
    
    public init(customerId: String?, cmd: String?, token: String? = nil) {
        self.customerId = customerId
        self.cmd = cmd
        self.token = token
    }
}
