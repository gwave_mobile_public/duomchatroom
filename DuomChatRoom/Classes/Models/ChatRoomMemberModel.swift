//
//  UserModel.swift
//  Duom
//
//  Created by kuroky on 2022/9/14.
//

import Foundation
import RxDataSources
import DuomNetworkKit
import HandyJSON

public enum ChatRoomRole: String, JSONCodableEnum {
    case owner = "OWNER"
    case manager = "ADMIN"
    case speaker = "SPEAKER"
    case listener = "LISTENER"
}

public enum MicStatus: String, JSONCodableEnum {
    case disConnect = "CLOSE"
    case connect = "CONNECT"
}

public struct ChatRoomMemberModel: JSONCodable {
    var role: ChatRoomRole = .listener
    var groupMute: Bool = false
    var micStatus: MicStatus = .connect
    var customerId: String?
    var nickName: String?
    var avatar: String?
    var isTalking: Bool = false
    
    public init() {}
    
    public init(role: ChatRoomRole = .listener,
                groupMute: Bool = false,
                micStatus: MicStatus = .disConnect,
                customerId: String? = nil,
                name: String? = nil,
                avatar: String? = nil,
                isTalking: Bool = false) {
        self.role = role
        self.groupMute = groupMute
        self.micStatus = micStatus
        self.customerId = customerId
        self.nickName = name
        self.avatar = avatar
        self.isTalking = isTalking
    }
    
    mutating func switchTalking(isTalking: Bool)  {
        self.isTalking = isTalking
    }
}

//  预览列表 成员model
public struct ChatRoomPreDetailModel: JSONCodable {
    var audienceCount: Int = 0
    var micMembers: [ChatRoomMemberModel] = []
    
    public init() {}
}


// MARK: business
/// 成员列表 collectionView
public struct MemberSection {
    public var header: String
    public var items: [ChatRoomMemberModel]
}

extension MemberSection: SectionModelType {
    var identity: String {
        return header
    }
    
    public init(original: MemberSection, items: [ChatRoomMemberModel]) {
        self = original
        self.items = items
    }
}


///  Guest 成员列表 tableView
public struct GuestSection {
    public var items: [GusetModel]
}

// 成员列表分区 Type
public enum MemberRoleType {
    case all
    case host
    case co_host
    case speaker
    case listener
}

public enum GusetModel {
    case header(type: MemberRoleType, count: Int, leftCount: Int)
    case user(type: MemberRoleType, model: ChatRoomMemberModel)
}

extension GuestSection: SectionModelType {
    public init(original: GuestSection, items: [GusetModel]) {
        self = original
        self.items = items
    }
}


