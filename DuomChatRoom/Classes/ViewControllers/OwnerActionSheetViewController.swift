//
//  OwnerActionSheetViewController.swift
//  Duom
//
//  Created by kuroky on 2022/9/15.
//

import UIKit
import DuomBase

// 对哪种角色处理
public enum ActionType {
    case toManager
    case toSpeaker
    case toListener
    case toSelf
}

public class OwnerActionSheetViewController: PresentationViewController {
    private(set) var sourceAction1:(()->Void)?
    
    private(set) var sourceAction2:(()->Void)?
    
    private(set) var titles: [String] = [String]()
    
    private let containerView: UIView = UIView().then {
        $0.backgroundColor = UIColor.themeLineColor
        $0.roundCorners(radius: 15)
    }
    
    private lazy var eventButton1 = DuomThemeButton.init(style: .white(mask: false), title: titles.first ?? "")
    
    private lazy var eventButton2 = DuomThemeButton.init(style: .white(mask: false), title: titles.last ?? "")
    
    private lazy var cancelButton = DuomThemeButton.init(style: .white(mask: false), title: "CANCEL")
    
    private let itemHeight = 54.DScale
    
    convenience init(type: ActionType, micStatus: MicStatus? = .connect, eventSource1: (()->Void)? = nil, eventSource2: (()->Void)? = nil) {
        self.init(nibName: nil, bundle: nil)
        self.sourceAction1 = eventSource1
        self.sourceAction2 = eventSource2
        switch type {
        case .toListener:
            titles = [CRConfig.default().toSpeakerAction]
        case .toSpeaker:
            if micStatus == .connect {
                titles = [CRConfig.default().removeSpeakerAction]
            } else {
                titles = [CRConfig.default().removeSpeakerAction]
            }
        case .toManager:
            titles = micStatus == .connect ? [CRConfig.default().muteUserAction] : [CRConfig.default().unMuteUserAction]
        case .toSelf:
            titles = micStatus == .connect ? [CRConfig.default().muteUserAction] : [CRConfig.default().unMuteUserAction]
        }
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        bindActions()
    }
    
    public override func setupConstraints() {
        super.setupConstraints()
        view.backgroundColor = .clear
        view.addSubview(containerView)
        containerView.addSubview(eventButton1)
        if titles.count > 1 {
            containerView.addSubview(eventButton2)
        }
        view.addSubview(cancelButton)
        containerView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalTo(HorizationMargin)
            make.trailing.equalTo(-HorizationMargin)
            make.height.equalTo(titles.count > 1 ? 109.DScale : 55.DScale)
            make.width.equalTo(UIScreen.screenWidth - (HorizationMargin * 2))
        }
        
        eventButton1.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(itemHeight)
        }
        
        if titles.count > 1 {
            eventButton2.snp.makeConstraints { make in
                make.bottom.leading.trailing.equalToSuperview()
                make.height.equalTo(itemHeight)
            }
        }
        
        cancelButton.snp.makeConstraints { make in
            make.top.equalTo(containerView.snp.bottom).offset(8.DScale)
            make.height.equalTo(itemHeight)
            make.leading.equalTo(HorizationMargin)
            make.trailing.equalTo(-HorizationMargin)
            make.bottom.equalTo(view.snp.bottom).offset(-20.DScale)
        }
        cancelButton.roundCorners(radius: 15)
    }
    
    private func bindActions()  {
        eventButton1.rx.tap
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                if let sourceAction1 = self.sourceAction1 {
                    sourceAction1()
                }
                self.dismiss(animated: true)
            })
            .disposed(by: disposeBag)
        
        eventButton2.rx.tap
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                if let sourceAction2 = self.sourceAction2 {
                    sourceAction2()
                }
                self.dismiss(animated: true)
            })
            .disposed(by: disposeBag)
        
        cancelButton.rx.tap
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.dismiss(animated: true)
            })
            .disposed(by: disposeBag)
    }


}
