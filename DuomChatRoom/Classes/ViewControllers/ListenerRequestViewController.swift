//
//  ListenerRequestViewController.swift
//  Duom
//
//  Created by kuroky on 2022/9/15.
//

import UIKit
import DuomBase

class ListenerRequestViewController: PresentationViewController, ViewType, ResponderChainRouter, ChatRoomSubModule {
    typealias Event = ChatRoomViewModel.Action
    typealias TargetViewModel = ChatRoomViewModel
    
//    private let containerView: UIView = UIView().then {
//        $0.backgroundColor = ThemeBlackColor
//    }
    private let micIconButton: UIButton = UIButton().then {
        $0.setImage(UIImage(named: "chat_request_icon"), for: .normal)
        $0.setImage(UIImage(named: "chat_request_icon"), for: .selected)
    }
    
    private let infoLabel: UILabel = UILabel().then {
        $0.textColor = UIColor.textWhiteColor
        $0.numberOfLines = 0
        $0.font = UIFont.appFont(ofSize: 20, fontType: .GT)
        $0.textAlignment = .center
        $0.lineBreakMode = .byWordWrapping
        $0.text = CRConfig.default().requestToSpeakerTitle
    }
    
    private let detailLabel: UILabel = UILabel().then {
        $0.textColor = UIColor(201, 201, 201)
        $0.numberOfLines = 0
        $0.font = UIFont.appFont(ofSize: 14, fontType: .Inter_Medium)
        $0.textAlignment = .center
        $0.lineBreakMode = .byWordWrapping
        $0.text = CRConfig.default().requestToSpeakaderDetail
    }
    
    private let requestButton: DuomThemeButton = DuomThemeButton.init(style: .highLight, title: "REQUSTE TO SPEAK")
    
    private let cancleButton: DuomThemeButton = DuomThemeButton.init(style: .normal, title: "CANCEL")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getChatRoomViewModel()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.view.backgroundColor = UIColor.themeBlackColor
        self.view.setupCorners(rect: view.bounds, corners: [.topLeft, .topRight], radii: CGSize(width: 10, height: 10))
        view.addSubview(micIconButton)
        view.addSubview(infoLabel)
        view.addSubview(detailLabel)
        view.addSubview(requestButton)
        view.addSubview(cancleButton)
        
        micIconButton.snp.makeConstraints { make in
            make.top.equalTo(50.DScale)
            make.centerX.equalToSuperview()
            make.size.equalTo(CGSize(width: 30.DScale, height: 30.DScale))
        }
        
        infoLabel.snp.makeConstraints { make in
            make.top.equalTo(micIconButton.snp.bottom).offset(24.DScale)
            make.leading.equalTo(16.DScale)
            make.trailing.equalTo(-16.DScale)
        }
        
        detailLabel.snp.makeConstraints { make in
            make.top.equalTo(infoLabel.snp.bottom).offset(16.DScale)
            make.leading.equalTo(16.DScale)
            make.trailing.equalTo(-16.DScale)
        }
        
        requestButton.snp.makeConstraints { make in
            make.top.equalTo(detailLabel.snp.bottom).offset(80.DScale)
            make.leading.equalTo(HorizationMargin)
            make.trailing.equalTo(-HorizationMargin)
            make.height.equalTo(36.DScale)
        }
        
        cancleButton.snp.makeConstraints { make in
            make.top.equalTo(requestButton.snp.bottom).offset(10.DScale)
            make.leading.equalTo(HorizationMargin)
            make.trailing.equalTo(-HorizationMargin)
            make.height.equalTo(36.DScale)
            make.width.equalTo(343.DScale)
            make.bottom.equalTo(view.snp.bottom).offset(-55.DScale)
        }
        
        cancleButton.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.dismiss(animated: true)
            })
            .disposed(by: disposeBag)
        
    }
    
    func bind(viewModel: ChatRoomViewModel) {
        requestButton.rx.tap
            .do(onNext: { [weak self] in
                self?.dismiss(animated: true)
            })
            .map { _ in .sendMsgToBeSpeaker }
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
        
        
        events
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
    }

}
