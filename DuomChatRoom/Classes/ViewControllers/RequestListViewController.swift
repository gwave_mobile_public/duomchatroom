//
//  RequestListViewController.swift
//  Duom
//
//  Created by kuroky on 2022/9/16.
//

import UIKit
import DuomBase
import RxDataSources
import RxCocoa
import RxSwift
import KKIMModule

/// 请求上麦列表
public struct RequesSection {
    public var items: [V2TIMMessage]
}

extension RequesSection: SectionModelType {
    public init(original: RequesSection, items: [V2TIMMessage]) {
        self = original
        self.items = items
    }
}

class RequestListViewController: PresentationViewController, ViewType, ResponderChainRouter, ChatRoomSubModule {
    typealias TargetViewModel = ChatRoomViewModel
    typealias Event = ChatRoomViewModel.Action
    
    private lazy var tableHeaderView: MemberListHeaderView = MemberListHeaderView(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.screenWidth, height: 88.DScale))
    
    private lazy var tableView = UITableView().then {
        $0.backgroundColor = UIColor.themeBlackColor
        $0.rowHeight = UITableView.automaticDimension
        $0.separatorStyle = .none
        $0.showsVerticalScrollIndicator = true
        $0.showsHorizontalScrollIndicator = false
        $0.contentInsetAdjustmentBehavior = .never
        $0.register(RequestListCell.self, forCellReuseIdentifier: RequestListCell.identifier)
        $0.tableHeaderView = tableHeaderView
    }
    
    private var items = PublishSubject<[RequesSection]>()
    
    private var dataList = [V2TIMMessage]()
    
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<RequesSection> (configureCell: { [weak self]
        (dataSource, tv, indexPath, element) in
        let cell = tv.dequeueReusableCell(withIdentifier: RequestListCell.identifier, for: indexPath) as! RequestListCell
        cell.configCell(element)
        cell.agreeAction = { [weak self] targetModel in
            guard let self = self else { return }
            self.viewModel?.action.onNext(.agreeToSpeaker(agree: true, message: targetModel))
        }
        cell.disAgreeAction = { targetModel in
            guard let self = self else { return }
            self.viewModel?.action.onNext(.agreeToSpeaker(agree: false, message: targetModel))
        }
        return cell
    })
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getChatRoomViewModel()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            make.height.equalTo(442.DScale)
            make.width.equalTo(UIScreen.screenWidth)
        }
    }
    
    func bind(viewModel: ChatRoomViewModel) {
        items.bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        dealDataList(viewModel.requesterList)
        
        viewModel.state.requestListPublish
            .subscribe(onNext: { [weak self] list in
                self?.dealDataList(list)
            })
            .disposed(by: disposeBag)
        
        events
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
    }
    
    private func dealDataList(_ list: [V2TIMMessage])  {
        self.dataList = list
        let section = RequesSection.init(items: dataList)
        items.onNext([section])
    }
    
}
