//
//  GuestsListViewController.swift
//  Duom
//
//  Created by kuroky on 2022/9/15.
//

import Foundation
import UIKit
import DuomBase
import RxDataSources
import RxCocoa
import RxSwift
import SwiftyImage

class GuestSegmentButton: UIButton {
    convenience init(title: String) {
        self.init()
        
        self.setTitle(title, for: .normal)
        self.setTitleColor(UIColor.textWhiteColor, for: .normal)
        self.setTitleColor(UIColor.themeBlackColor, for: .selected)
        self.setBackgroundImage(UIImage.resizable().color(UIColor.themeBlackColor).corner(radius: 24.DScale/2).border(color: UIColor.white).border(width: 1).image, for: .normal)
        self.setBackgroundImage(UIImage.resizable().color(UIColor.themeColor).corner(radius: 24.DScale/2).image, for: .selected)
    }
}

class ListTitleHeaderView: BaseView {
    private let headerView = MemberListHeaderView()
    
    private let containerView = UIView(backgroundColor: .clear)
    
    private let allButton = GuestSegmentButton(title: "All")
    
    private let hostsButton = GuestSegmentButton(title: "Co-hosts")
    
    private let speakerButton = GuestSegmentButton(title: "Speakers")
    
    private let listenerButton = GuestSegmentButton(title: "Listeners")
    
    var typeChangeBlock: ((MemberRoleType) -> Void)?
    
    var clickLeaveAction: (() -> Void)?
    
    private let disposeBag = DisposeBag()
    
    private(set) var currentType: MemberRoleType = .all {
        didSet {
            switch currentType {
            case .all:
                self.allButton.isSelected = true
                self.hostsButton.isSelected = false
                self.speakerButton.isSelected = false
                self.listenerButton.isSelected = false
            case .host:
                break
            case .co_host:
                self.allButton.isSelected = false
                self.hostsButton.isSelected = true
                self.speakerButton.isSelected = false
                self.listenerButton.isSelected = false
            case .speaker:
                self.allButton.isSelected = false
                self.hostsButton.isSelected = false
                self.speakerButton.isSelected = true
                self.listenerButton.isSelected = false
            case .listener:
                self.allButton.isSelected = false
                self.hostsButton.isSelected = false
                self.speakerButton.isSelected = false
                self.listenerButton.isSelected = true
            }
            
            if let typeChangeBlock = self.typeChangeBlock {
                typeChangeBlock(currentType)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        bingAction()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setupUI() {
        super.setupUI()
        backgroundColor = UIColor.themeBlackColor
        addSubview(headerView)
        addSubview(containerView)
        
        headerView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.height.equalTo(64.DScale)
        }
        
        containerView.snp.makeConstraints { make in
            make.leading.equalTo(12.DScale)
            make.trailing.equalTo(-12.DScale)
            make.top.equalTo(headerView.snp.bottom).offset(27.DScale)
            make.bottom.equalTo(-24.DScale)
        }
        
        allButton.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 43.DScale, height: 24.DScale))
        }
        
        hostsButton.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 93.DScale, height: 24.DScale))
        }
        
        speakerButton.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 93.DScale, height: 24.DScale))
        }
        
        listenerButton.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 93.DScale, height: 24.DScale))
        }
        
        containerView.hstack(allButton, hostsButton, speakerButton, listenerButton,
                    spacing: 12.DScale,
                    alignment: .leading,
                    distribution: .equalSpacing)
        
        self.currentType = .all
    }
    
    private func bingAction() {
        headerView.leaveAction = { [weak self] in
            if let action = self?.clickLeaveAction {
                action()
            }
        }
        
        allButton.rx.tap
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.currentType = .all
                if let typeChangeBlock = self.typeChangeBlock {
                    typeChangeBlock(.all)
                }
            })
            .disposed(by: disposeBag)
        
        hostsButton.rx.tap
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.currentType = .co_host
                if let typeChangeBlock = self.typeChangeBlock {
                    typeChangeBlock(.co_host)
                }
            })
            .disposed(by: disposeBag)
        
        speakerButton.rx.tap
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.currentType = .speaker
                if let typeChangeBlock = self.typeChangeBlock {
                    typeChangeBlock(.speaker)
                }
            })
            .disposed(by: disposeBag)
        
        listenerButton.rx.tap
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.currentType = .listener
                if let typeChangeBlock = self.typeChangeBlock {
                    typeChangeBlock(.listener)
                }
            })
            .disposed(by: disposeBag)
    }
    
    
    func updateHeaderCount(_ count: Int)  {
        self.headerView.updateCount(count)
    }
}
 


class GuestsListViewController: PresentationViewController, ViewType, ChatRoomSubModule {
    typealias TargetViewModel = ChatRoomViewModel
    
    private lazy var headerView: ListTitleHeaderView = ListTitleHeaderView()
    
    private var dataList = [ChatRoomMemberModel]()
    private var coHostSecion = GuestSection(items: [GusetModel]())
    private var speakerSection = GuestSection(items: [GusetModel]())
    private var listenerSection = GuestSection(items: [GusetModel]())
    
    private var items = PublishSubject<[GuestSection]>()
    
    
    private lazy var tableView = UITableView().then {
        $0.backgroundColor = UIColor.themeBlackColor
        $0.rowHeight = UITableView.automaticDimension
        $0.separatorStyle = .none
        $0.showsVerticalScrollIndicator = true
        $0.showsHorizontalScrollIndicator = false
        $0.contentInsetAdjustmentBehavior = .never
        $0.register(GuestsListTableViewCell.self, forCellReuseIdentifier: GuestsListTableViewCell.identifier)
        $0.register(GuestsCountTableViewCell.self, forCellReuseIdentifier: GuestsCountTableViewCell.identifier)
        $0.emptyView = EmptyView.init(.normal(title: "No Data"))
    }
    
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<GuestSection> (configureCell: { [weak self]
        (dataSource, tv, indexPath, element) in
        switch dataSource[indexPath] {
        case let .header(type, count, leftCount):
            let cell = tv.dequeueReusableCell(withIdentifier: GuestsCountTableViewCell.identifier, for: indexPath) as! GuestsCountTableViewCell
            cell.configCell(role: self?.viewModel?.role ?? .listener, groupMute: self?.dataList.first?.groupMute ?? false, type: type, count: count, leftCount: leftCount)
            cell.muteAction = { mute in
                if mute {
                    self?.viewModel?.action.onNext(.muteAll)
                } else {
                    self?.viewModel?.action.onNext(.unMuteAll)
                }
            }
            return cell
        case let .user(type, model):
            let cell = tv.dequeueReusableCell(withIdentifier: GuestsListTableViewCell.identifier, for: indexPath) as! GuestsListTableViewCell
            cell.configCell(role: self?.viewModel?.role ?? .listener, type: type, mode: model)
            return cell
        }
    })
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getChatRoomViewModel()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        view.backgroundColor = UIColor.themeBlackColor
        view.addSubview(headerView)
        view.addSubview(tableView)
        
        headerView.snp.makeConstraints { make in
            make.leading.top.trailing.equalToSuperview()
            make.height.equalTo(139.DScale)
            make.width.equalTo(UIScreen.screenWidth)
        }
        
        var addHeight = CGFloat(1) * 72.DScale
        addHeight = min(addHeight, 200.DScale)
        tableView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide)
            make.top.equalTo(headerView.snp.bottom)
            make.height.equalTo(304.DScale + addHeight)
        }
        
        headerView.typeChangeBlock = { [weak self] currentType in
            guard let self = self else { return }
            switch currentType {
            case .all:
                self.items.onNext([self.coHostSecion, self.speakerSection, self.listenerSection])
            case .host:
                break
            case .co_host:
                self.items.onNext([self.coHostSecion])
            case .speaker:
                self.items.onNext([self.speakerSection])
            case .listener:
                self.items.onNext([self.listenerSection])
            }
        }
        
        headerView.clickLeaveAction = { [weak self] in
            self?.dismiss(animated: true)
        }
    }
    
    func bind(viewModel: ChatRoomViewModel) {
        dataList = viewModel.memberList
        
        viewModel.state.memberListData
            .subscribe(onNext: { [weak self] list in
                self?.dataList = list
                self?.dealDataSource()
            })
            .disposed(by: disposeBag)
        
        items
            .do(onNext: { [weak self] sections in
                var count = 0
                sections.forEach {
                    count += ($0.items.count - 1) // 除去header
                }
                self?.updateHeaderCounts(count)
            })
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        self.dealDataSource()
    }
    
    
    private func dealDataSource()  {
        guard dataList.isNotEmpty else { return }
        
        coHostSecion.items.removeAll()
        speakerSection.items.removeAll()
        listenerSection.items.removeAll()
        
        dataList.forEach {
            switch $0.role {
            case .manager:
                coHostSecion.items.append(GusetModel.user(type: .co_host, model: $0))
            case .speaker:
                speakerSection.items.append(GusetModel.user(type: .speaker, model: $0))
            case .listener:
                listenerSection.items.append(GusetModel.user(type: .listener, model: $0))
            default:
                break
            }
        }
        
        coHostSecion.items.insert(GusetModel.header(type: .co_host, count: coHostSecion.items.count, leftCount: CRConfig.default().coHostMaxCount - coHostSecion.items.count), at: 0)
        speakerSection.items.insert(GusetModel.header(type: .speaker, count: speakerSection.items.count, leftCount: CRConfig.default().speakersMaxCount - coHostSecion.items.count), at: 0)
        listenerSection.items.insert(GusetModel.header(type: .listener, count: listenerSection.items.count, leftCount: 0 ), at: 0)
        
        items.onNext([coHostSecion, speakerSection, listenerSection])
    }
    
    private func updateHeaderCounts(_ count: Int) {
        self.headerView.updateHeaderCount(count)
    }
    
}
