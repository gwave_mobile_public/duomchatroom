//
//  ChatRoomMainViewController.swift
//  Duom
//
//  Created by kuroky on 2022/9/13.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift
import DuomBase

public class ChatRoomMainViewController: PresentationViewController, ViewType, ResponderChainRouter, ChatRoomSubModule {
    public typealias Event = ChatRoomViewModel.Action
    
    private let containerView: UIView = UIView().then {
        $0.backgroundColor = UIColor.themeBlackColor
    }
    
    public var role: ChatRoomRole = .listener
    
    private lazy var headerView: ChatRoomHeaderView = ChatRoomHeaderView(role: self.role)
    
    private lazy var collectionViewController: MemberListCollectionController = MemberListCollectionController()
    
    private lazy var tipView: ChatRoomEnterTipView = ChatRoomEnterTipView()
    
    private lazy var toolsView: ChatRoomToolsView = ChatRoomToolsView().then {
        $0.role = self.role
        $0.isHidden = true
    }
    
    private let itemWidth = (UIScreen.screenWidth - 16.DScale * 2 - 21.DScale * 3) / 4
   
    public override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        
        // 注入当前ViewModel
        subModuleService.inject(viewModel)
        
        addViewAction()
    }
    
    public override func setupConstraints() {
        super.setupConstraints()
        
        self.view.addSubview(containerView)
        containerView.addSubview(headerView)
        addChild(collectionViewController)
        containerView.addSubview(collectionViewController.view)
        collectionViewController.didMove(toParent: self)
        
        containerView.addSubview(tipView)
        containerView.addSubview(toolsView)
        
        containerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(108.DScale)
            make.bottom.equalTo(view.snp.bottom)
            make.height.equalTo(704.DScale)
        }
        containerView.setupCorners(rect: view.bounds, corners: [.topLeft, .topRight], radii: CGSize(width: 5, height: 5))
        
        headerView.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.width.equalTo(UIScreen.screenWidth)
            make.height.equalTo((self.role == .owner || self.role == .manager) ? 110.DScale : 62.DScale)
        }

        collectionViewController.view.snp.makeConstraints { make in
            make.top.equalTo(headerView.snp.bottom)
            make.trailing.leading.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        tipView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(153.DScale)
            make.bottom.equalTo(self.view)
        }
        
        toolsView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
            make.height.equalTo(110)
        }
//        toolsView.layoutIfNeeded()
//        toolsView.gradient(colors: [.themeBlackColor, .themeBlackColor65])
        
        if self.role == .owner || self.role == .manager {
            self.showToolsView()
        } 
        
        headerView.configHeaderView(viewModel?.roomName, topic: viewModel?.roomTopic)
        toolsView.updateMessageCount(viewModel?.requesterList.count ?? 0)
    }
    
    public func bind(viewModel: ChatRoomViewModel) {
        self.role = viewModel.role

        rx.viewWillAppear
            .flatMap { _ in Observable.of(.startEvent, .adminJoinRoom) }
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
        
        viewModel.state.leaveRoomPublish
            .subscribe(onNext: { [weak self] in
                self?.dismiss(animated: true)
            })
            .disposed(by: disposeBag)
        
        viewModel.state.selfMicStatus
            .subscribe(onNext: { [weak self] micIsOff in
                self?.toolsView.updateMicStatus(micIsOff)
            })
            .disposed(by: disposeBag)
        
        viewModel.state.beSpeakerPublish
            .subscribe(onNext: { [weak self] isSpeaker in
                self?.toolsView.updateSpeakerStatus(isSpeaker)
            })
            .disposed(by: disposeBag)
        
        viewModel.state.roomTitlePublish
            .subscribe(onNext: { [weak self] title in
                self?.headerView.updateTitle(title)
            })
            .disposed(by: disposeBag)
        
        viewModel.state.allMuteStatus
            .subscribe(onNext: { [weak self] isMute in
                self?.toolsView.updateGroupMuteStatu(isMute)
                self?.toolsView.updateMicStatus(isMute)
            })
            .disposed(by: disposeBag)
        
        viewModel.state.requestListPublish
            .subscribe(onNext: { [weak self] list in
                self?.toolsView.updateMessageCount(list.count)
            })
            .disposed(by: disposeBag)
    
        viewModel.state.groupIsDismiss
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.isShowingMiniChatRoomOnWindow() ? self.dismiss(animated: true) : self.showBeEndAlert()
            })
            .disposed(by: disposeBag)
        
        viewModel.state.endRoomPublish
            .subscribe(onNext: { [weak self] in
                self?.dismiss(animated: true)
            })
            .disposed(by: disposeBag)
        
        
        events
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
    }
}


extension ChatRoomMainViewController {
    private func addViewAction()  {
        headerView.leaveAction = { [weak self] in
            guard let self = self else { return }
            if let vm = self.viewModel {
                if self.role == .owner {
                    self.showEndAlert()
                } else {
                    if !vm.isOnGroup {
                        self.dismiss(animated: true)
                    } else {
                        self.showLeaveAlert()
                    }
                }
            }
        }
        
        headerView.modifyAction = { [weak self] newTitle in
            self?.viewModel?.action.onNext(.modifyTopic(title: newTitle))
        }
        
        headerView.miniSizeAction = { [weak self] in
            guard let self = self else { return }
            if let vm = self.viewModel, vm.isOnGroup {
                self.hiddenCurrentChainsViews(true)
                CRSharedStreamWindow.shared().showChatRoomToWindow(self.viewModel, roomVC: self)
            } else {
                self.dismiss(animated: true)
            }
        }
        
        tipView.startAction = { [weak self] in
            guard let self = self else { return }
            self.viewModel?.action.onNext(.joinChatRoom)
            self.showToolsView()
        }
    }
    
    private func showLeaveAlert() {
        let alert = ChatRoomAlert(title: CRConfig.default().leaveRoomTitle,
                                  message: CRConfig.default().leaveRoomMessage,
                                  cancelTitle: "CANCEL",
                                  doneTitle: "LEAVE")
        alert.leaveAction = { [weak self] _ in
            self?.viewModel?.action.onNext(.leaveRoom)
        }
        alert.cancelAction = { _ in
            print("cancel")
        }
        self.present(alert, animated: true)
    }
    
    // 主播主动结束
    private func showEndAlert() {
        let alert = ChatRoomAlert(title: CRConfig.default().endRoomTitle,
                                  message: CRConfig.default().endRoomAuthorMessage,
                                  cancelTitle: "CANCEL",
                                  doneTitle: "END")
        alert.leaveAction = { [weak self] _ in
            self?.viewModel?.action.onNext(.endRoom)
        }
        alert.cancelAction = { _ in
            print("cancel")
        }
        self.present(alert, animated: true)
    }
    
    
    // 用户收到主播结束
    private func showBeEndAlert() {
        let alert = ChatRoomAlert(title: CRConfig.default().endRoomTitle,
                                  message: CRConfig.default().endRoomAuthorMessage,
                                  doneTitle: "OK")
        alert.leaveAction = { [weak self] _ in
            self?.dismiss(animated: true)
        }
        self.present(alert, animated: true)
    }
    
    private func showToolsView()  {
        tipView.isHidden = true
        toolsView.isHidden = false
    }
}

extension ChatRoomMainViewController {
    func hiddenCurrentChainsViews(_ hidden: Bool)  {
        var topView: UIView = self.view
        self.view.isHidden = hidden
        while !(topView.superview is UIWindow) {
            if let nextView = topView.superview {
                nextView.isHidden = hidden
                topView = nextView
            }
        }
    }
    
    private func isShowingMiniChatRoomOnWindow() -> Bool {
      if let keyWindow = UIApplication.getWindow(), keyWindow.subviews.contains(where: { $0 is ChatRoomWindowView }) {
        return true
      }
      return false
    }
}
