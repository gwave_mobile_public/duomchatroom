//
//  MemberListCollectionViewController.swift
//  Duom
//
//  Created by kuroky on 2022/9/13.
//  ChatRoom 成员列表
//

import UIKit
import DuomBase
import RxDataSources
import RxSwift
import RxCocoa
import RxViewController

class MemberListCollectionController: BaseViewController, ViewType, ResponderChainRouter, ChatRoomSubModule {
    typealias Event = ChatRoomViewModel.Action
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewLayout()).then {
        $0.register(MemberCollectionViewCell.self, forCellWithReuseIdentifier: MemberCollectionViewCell.identifier)
        $0.register(MemberListRoleHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: MemberListRoleHeaderView.identifier)
        $0.backgroundColor = .clear
        $0.showsVerticalScrollIndicator = false
        $0.showsHorizontalScrollIndicator = false
        $0.emptyView = EmptyView(.normal(title: "No Data"))
    }
    
    private let memberListLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout().then {
        let width = (UIScreen.screenWidth - 15.DScale * 2 - 21.DScale * 3) / 4
        $0.itemSize = CGSize(width: width, height: width + 25.DScale)
        $0.minimumLineSpacing = 10
        $0.minimumInteritemSpacing = 21.DScale
        $0.scrollDirection = .vertical
        $0.sectionInset = UIEdgeInsets.init(top: 0, left: 15.DScale, bottom: 15, right: 15.DScale)
        $0.headerReferenceSize = CGSize(width: UIScreen.screenWidth, height: 45)
        $0.footerReferenceSize = .zero
    }
    
    private let dataSource = RxCollectionViewSectionedReloadDataSource<MemberSection>(configureCell: {
        (datasource, collect, idx, element) in
        let cell = collect.dequeueReusableCell(withReuseIdentifier: MemberCollectionViewCell.identifier, for: idx) as! MemberCollectionViewCell
        cell.configWithModel(element)
        return cell
    }, configureSupplementaryView: { (datasource, collect, kind, idx) in
        let sectionHeader = collect.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: MemberListRoleHeaderView.identifier, for: idx) as! MemberListRoleHeaderView
        sectionHeader.configType("\(datasource[idx.section].header)")
        return sectionHeader
    })
    
    private var hostSecion = MemberSection(header: CRConfig.default().hostRole, items: [ChatRoomMemberModel]())
    private var coHostSecion = MemberSection(header: CRConfig.default().coHostRole, items: [ChatRoomMemberModel]())
    private var speakerSection = MemberSection(header: CRConfig.default().speakerRole, items: [ChatRoomMemberModel]())
    private var listenerSection = MemberSection(header: CRConfig.default().listenerRole, items: [ChatRoomMemberModel]())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.themeBlackColor
        collectionView.collectionViewLayout = memberListLayout
        
        getChatRoomViewModel()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    func bind(viewModel: ChatRoomViewModel) {
        viewModel.refresher = collectionView.refresher
        collectionView.refresher?.setting(style: .light, type: .footer)
        
        if viewModel.role != .owner {
            rx.viewWillAppear
                .map { _ in .preRoomDetail }
                .bind(to: viewModel.action)
                .disposed(by: disposeBag)
        }
        
        collectionView.rx.footerRefresh
            .map {  _ in .requestMoreMember}
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
        
        viewModel.state.memberListData
            .do(onNext: { [weak self]  list in
                guard let self = self else { return }
                self.dealDataSource(list)
            })
            .flatMapLatest{ [weak self] list -> Observable<[MemberSection]> in
                guard let self = self else { return .empty() }
                return .just([self.hostSecion, self.coHostSecion, self.speakerSection, self.listenerSection].filter { $0.items.isNotEmpty })
            }
            .bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        collectionView.rx.modelSelected(ChatRoomMemberModel.self)
            .subscribe(onNext: { [weak self] model in
            // 当前为听众不需要有任何操作
            if viewModel.role == .listener {
                return
            }
            
            // 其他人不能对owner操作
            if viewModel.userId != model.customerId, model.role == .owner {
                return
            }
            
            var type: ActionType = .toListener
            if viewModel.userId == model.customerId {
                type = .toSelf
            } else if  model.role == .listener {
                type = .toListener
            } else if model.role == .speaker {
                type = .toSpeaker
            } else if model.role == .manager {
                if viewModel.role == .owner {
                    type = .toManager
                }
            }
            
            let vc = OwnerActionSheetViewController(type: type, micStatus: model.micStatus, eventSource1: {
                // 自己操作自己 禁言
                if type == .toSelf {
                    if model.micStatus == .connect {
                        if model.groupMute == true {
                            toast("group is muteing")
                        } else {
                            viewModel.action.onNext(.muteSelf)
                        }
                    } else {
                        if model.groupMute == true {
                            toast("group is muteing")
                        } else {
                            viewModel.action.onNext(.unMuteSelf)
                        }
                    }
                } else if type == .toListener {
                    // set 上麦
                    viewModel.action.onNext(.pickUserToSpeaker(targetId: model.customerId ?? ""))
                } else if type == .toSpeaker {
                    // 下麦
                    viewModel.action.onNext(.kickSomeone(targetId: model.customerId ?? ""))
                } else {
                    //                        if model.micStatus == .connect {
                    //                            // 禁麦
                    //                            viewModel.action.onNext(.muteSomeone(targetId: model.customerId ?? ""))
                    //                        } else {
                    //                            // 解禁
                    //                            viewModel.action.onNext(.unMuteSomeone(targetId: model.customerId ?? ""))
                    //                        }
                }
                
            })
            self?.present(vc, animated: true)
        })
        .disposed(by: disposeBag)
        
//        viewModel.state.volumeSpeakerPublish
//            .subscribe(onNext: { [weak self] uid in
//                guard let self = self else { return }
//                for (index, section) in [self.hostSecion, self.coHostSecion, self.speakerSection].enumerated() {
//                    for (idx, item) in section.items.enumerated() {
//                        if item.customerId == uid {
//                            let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: MemberCollectionViewCell.identifier, for: IndexPath.init(row: idx, section: index)) as! MemberCollectionViewCell
//                            cell.startTalking()
//                        }
//                    }
//                }
//                
//            })
//            .disposed(by: disposeBag)
        
        events
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)
    }
    
    private func dealDataSource(_ datas: [ChatRoomMemberModel])  {
        guard datas.isNotEmpty else { return }
        
        hostSecion.items.removeAll()
        coHostSecion.items.removeAll()
        speakerSection.items.removeAll()
        listenerSection.items.removeAll()
        
        datas.forEach {
            switch $0.role {
            case .owner:
                hostSecion.items.append($0)
            case .manager:
                coHostSecion.items.append($0)
            case .speaker:
                speakerSection.items.append($0)
            case .listener:
                listenerSection.items.append($0)
            default:
                break
            }
        }
    }
}

