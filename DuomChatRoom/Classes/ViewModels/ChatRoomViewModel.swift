//
//  ChatRoomViewModel.swift
//  DuomChatRoom
//
//  Created by kuroky on 2022/9/20.
//

import Foundation
import RxSwift
import RxCocoa
import DuomBase
import DuomNetworkKit
import KKIMModule

public class ChatRoomViewModel: ViewModel {
    public enum Action: ResponderChainEvent {
        // 预览
        case preRoomDetail
        // 听众加入room
        case joinChatRoom
        // 听众离开room
        case leaveRoom
        // 主播开启room
        case startEvent
        // 修改活动话题
        case modifyTopic(title: String)
        // 抱用户上麦
        case pickUserToSpeaker(targetId: String)
        // 踢用户下麦
        case kickSomeone(targetId: String)
        // 听众发消息请求上麦
        case sendMsgToBeSpeaker
        // 禁言某人
        case muteSelf
        // 解禁某人
        case unMuteSelf
        // 全部禁言
        case muteAll
        // 全部解禁
        case unMuteAll
        // 同意用户申请
        case agreeToSpeaker(agree: Bool, message: V2TIMMessage)
        // 主播结束room
        case endRoom
        // 管理员进入直播间
        case adminJoinRoom
        
        // 加载更多
        case requestMoreMember
    }
    
    public class State: Output {
        var preListMemberCount = PublishRelay<Int>()
        
        var memberListData = PublishSubject<[ChatRoomMemberModel]>()
        
        var leaveRoomPublish = PublishRelay<Void>()
        
        // 自己的开闭麦状态
        var selfMicStatus = PublishRelay<Bool>()
        
        // 自己被设置/取消 speaker了
        var beSpeakerPublish = PublishRelay<Bool>()
        
        // 群名称变化
        var roomTitlePublish = PublishRelay<String>()
        
        // 请求上麦列表信号
        var requestListPublish = PublishRelay<[V2TIMMessage]>()
        
        // 群组解散
        var groupIsDismiss = PublishRelay<Void>()
        
        // 主播结束活动
        var endRoomPublish = PublishRelay<Void>()
        
        // 声音触发
        var volumeSpeakerPublish = PublishRelay<String>()
        
        // 全体禁麦
        var allMuteStatus = PublishRelay<Bool>()
        
        required public init() {
        }
    }
    
    private let disposeBag = DisposeBag()
    
    public let session: ChatSessison?
    
    public var mediaKit: ChatAgoraMedia?
    
    var memberList: [ChatRoomMemberModel] = [ChatRoomMemberModel]()
    
    // 听众请求列表
    var requesterList: [V2TIMMessage] = [V2TIMMessage]()
    
    public var eventId: String = ""
    
    var userId: String?
    
    var roomName: String?
    
    var roomTopic: String?
    
    var groupID = ""
    
    var role: ChatRoomRole
    
    // 用户是否已经在group内
    private(set) var isOnGroup: Bool = false
    
    var refresher: Refresher?
    
    private(set) var currentPage: Int = 0
    
    public init(role: ChatRoomRole, eventId: String, userId: String, roomName: String?, roomTopic: String?) {
        self.role = role
        self.eventId = eventId
        self.userId = userId
        self.roomName = roomName
        self.roomTopic = roomTopic
        self.session = ChatSessison(role: role)
        self.mediaKit = ChatAgoraMedia(role: role)
        
        addSessionObservable()
        addMediaKitObservable()
    }
    
    private func addSessionObservable()  {
        self.session?.updateListObservable
            .subscribe(onNext: { [weak self] in
                self?.getGroupMember()
            })
            .disposed(by: disposeBag)
        
        self.session?.newmessageObservable
            .subscribe(onNext: { [weak self] msg in
                guard let self = self, let msg = msg else { return }
                self.dealReciveMessage(msg)
            })
            .disposed(by: disposeBag)
    }
    
    private func addMediaKitObservable()  {
        self.mediaKit?.activeSpeakerObservable
            .subscribe(onNext: { [weak self] speaker in
                guard let self = self else { return }
                switch speaker {
                case .local:
                    if let userid = self.userId, userid.isNotEmpty {
                        self.state.volumeSpeakerPublish.accept(userid)
                    }
                case let .other(agoraUid: agoraUid):
                    self.state.volumeSpeakerPublish.accept("\(agoraUid)")
                }
            })
            .disposed(by: disposeBag)
    }
    
    public func mutate(action: Action, state: State) {
        switch action {
        case .preRoomDetail:
            preRoomDetail(eventId)
        case .joinChatRoom:
            listenerJoinRoom()
        case .leaveRoom:
            listenerLeaveRoom()
        case .startEvent:
            startEventAction()
        case let .pickUserToSpeaker(targetId: targetId):
            pickUserToSpeaker(targetId: targetId)
        case let .kickSomeone(targetId: targetId):
            kickUserFromMic(targetId: targetId)
        case .sendMsgToBeSpeaker:
            sendMsgToSpeaker()
        case .muteSelf:
            muteSelfMic()
        case .unMuteSelf:
            unMuteSelfMic()
        case .muteAll:
            muteAllMic()
        case .unMuteAll:
            unMuteAllMic()
        case let .modifyTopic(title: title):
            modifyRoomTitle(title)
        case let .agreeToSpeaker(agree: agree, message: message):
            agreeToSpeaker(agree: agree, msg: message)
        case .endRoom:
            endRoomAction()
        case .adminJoinRoom:
            adminerJoinRoom()
        case .requestMoreMember:
            self.getGroupMember()
        }
    }
}

// MARK: Functions
extension ChatRoomViewModel {
    // 修改房间信息
    private func modifyRoomTitle(_ title: String)  {
        guard role == .owner || role == .manager else { return }
        chatRoomProvider.rx
            .request(.modifyRoomTitle(eventId: eventId, title: title))
            .map(ChatRoomInfoModel.self)
            .catchAndReturn(ChatRoomInfoModel())
            .showHUD()
            .subscribe(onSuccess: { [weak self] infoModel in
                if let event = infoModel.event, let title = event.title {
                    self?.state.roomTitlePublish.accept(title)
                }
            })
            .disposed(by: disposeBag)
    }
    
    // 预览房间
    private func preRoomDetail(_ cid: String)  {
        chatRoomProvider.rx
            .request(.preDetailList(cid: cid))
            .map(ChatRoomPreDetailModel.self)
            .catchAndReturn(ChatRoomPreDetailModel())
            .subscribe(onSuccess: { [weak self] preModel in
                guard let self = self else { return }
                self.memberList.removeAll()
                self.memberList += preModel.micMembers
                self.state.memberListData.onNext(self.memberList)
                self.state.preListMemberCount.accept(preModel.audienceCount)
            })
            .disposed(by: disposeBag)
    }
    
    
    // 明星开启活动
    private func startEventAction()  {
        guard role == .owner else { return }
        chatRoomProvider.rx
            .request(.startEvent(eventId: eventId))
            .map(ChatRoomJoinSuccessModel.self)
            .showHUD()
            .subscribe(onSuccess: { [weak self] joinModel in
                guard let self = self,
                      let groupId = joinModel.groupId,
                      let token = joinModel.token,
                      let channelId = joinModel.eventId,
                      let mediaKit = self.mediaKit else { return }
                self.groupID = groupId
                mediaKit.joinChannel(token, channelId: channelId, userID: self.userId ?? "0")
                mediaKit.muteLocalAudioStream(false)
                self.session?.addGroupListener()
                self.session?.addGroupMsgListener()
                self.isOnGroup = true
                // 获取群成员
                self.getGroupMember()
            }, onFailure: { obj in
                print("\(obj)")
            })
            .disposed(by: disposeBag)
    }
    
    // 加入房间
    private func listenerJoinRoom()  {
        chatRoomProvider.rx
            .request(.joinRoom(eventId: eventId))
            .map(ChatRoomJoinSuccessModel.self)
            .showHUD()
            .subscribe(onSuccess: { [weak self] joinModel in
                guard let self = self,
                      let groupId = joinModel.groupId,
                      let token = joinModel.token,
                      let channelId = joinModel.eventId,
                      let mediaKit = self.mediaKit else { return }
                self.groupID = groupId
                
                mediaKit.joinChannel(token, channelId: channelId, userID: self.userId ?? "0")
                self.session?.joinGroup(groupId)
                self.isOnGroup = true
                // 获取群成员
                self.getGroupMember()
            })
            .disposed(by: disposeBag)
    }
    
    // admin 加入房间
    private func adminerJoinRoom()  {
        guard role == .manager else { return }
        chatRoomProvider.rx
            .request(.joinRoom(eventId: eventId))
            .map(ChatRoomJoinSuccessModel.self)
            .showHUD()
            .subscribe(onSuccess: { [weak self] joinModel in
                guard let self = self,
                      let groupId = joinModel.groupId,
                      let token = joinModel.token,
                      let channelId = joinModel.eventId,
                      let mediaKit = self.mediaKit else { return }
                self.groupID = groupId
                
                mediaKit.joinChannel(token, channelId: channelId, userID: self.userId ?? "0")
                mediaKit.setClientRole(.broadcaster)
                mediaKit.muteLocalAudioStream(false)
                self.session?.joinGroup(groupId)
                self.isOnGroup = true
                // 获取群成员
                self.getGroupMember()
            })
            .disposed(by: disposeBag)
    }
    
    // 结束直播 关闭room
    private func endRoomAction()  {
        guard role == .owner || role == .manager else { return }
        chatRoomProvider.rx
            .request(.finishEvent(eventId: eventId))
            .mapJSON()
            .showHUD()
            .subscribe(onSuccess: { [weak self] json in
                guard let self = self else { return }
                self.groupID = ""
                if let mediaKit = self.mediaKit {
                    mediaKit.deinitAction()
                }
                if let session = self.session {
                    session.deinitAction()
                }
                NotificationCenter.default.post(name: NSNotification.Name("key_refresh_chatroom"), object: nil)
                self.state.endRoomPublish.accept(())
            })
            .disposed(by: disposeBag)
    }
    
    // 获取群成员列表
    private func getGroupMember()  {
        guard let session = session else { return }
        session.getGroupMemberList(groupID, page: currentPage) { [weak self] nextPage, models in
            guard let self = self else { return }
            if self.currentPage == 0 { // 只有首页需要请求麦位数据
                self.getMicSpeakerList(models)
            } else {
                self.memberList += models
                self.state.memberListData.onNext(self.memberList)
            }
            
            if nextPage > 0 {
                self.currentPage = nextPage
            }
            
            self.refresher?.endRefresh(with: nextPage == 0 ? .noMore : .normal)
        }
    }
    
    // 获取所有在麦位状态的成员 用来同步更新IM接口的成员列表role
    func getMicSpeakerList(_ imList: [ChatRoomMemberModel]) {
        chatRoomProvider.rx
            .request(.micDetailList(cid: eventId))
            .map([ChatRoomMemberModel].self)
            .subscribe(onSuccess: { [weak self] list in
                guard let self = self else { return }
                var firstPageList = [ChatRoomMemberModel]()
                imList.forEach { tempImModel in
                    var newModel = tempImModel
                    if let index = list.firstIndex(where: { $0.customerId == tempImModel.customerId }) {
                        newModel.groupMute = list[index].groupMute
                        newModel.micStatus = list[index].micStatus
                        //                        newModel.role = list[index].role
                        firstPageList.append(newModel)
                        self.publishSelfMicStatus(newModel)
                    } else {
                        firstPageList.append(tempImModel)
                    }
                }
                
                self.memberList.removeAll()
                self.memberList += firstPageList
                self.state.memberListData.onNext(self.memberList)
            })
            .disposed(by: disposeBag)
    }
    
    // 找出当前用户 发送麦状态信号
    private func publishSelfMicStatus(_ model: ChatRoomMemberModel)  {
        guard model.customerId == self.userId else { return }
        if model.groupMute {
            self.state.selfMicStatus.accept(true)
        } else {
            self.state.selfMicStatus.accept(model.micStatus == .disConnect)
        }
    }
    
    // 退出群
    private func listenerLeaveRoom()  {
        chatRoomProvider.rx
            .request(.leaveRoom(eventId: eventId))
            .mapJSON()
            .showHUD()
            .subscribe(onSuccess: { [weak self] json in
                guard let self = self,
                      let mediaKit = self.mediaKit else { return }
                // 闭麦 并退出
                mediaKit.setClientRole(.audience)
                mediaKit.muteLocalAudioStream(true)
                mediaKit.leaveChannel()
                
                self.session?.quitGroupAction(self.groupID)
                self.isOnGroup = false
                
                self.state.leaveRoomPublish.accept(())
            }, onFailure: { error in
                Logger.log(message: "leave chatRoom failure \(error.localizedDescription)", level: .warning)
            })
            .disposed(by: disposeBag)
    }
    
    
    // 抱用户上麦
    private func pickUserToSpeaker(targetId: String)  {
        guard role == .owner || role == .manager, !targetId.isEmpty else { return }
        chatRoomProvider.rx
            .request(.pickUserSpeak(cid: eventId, targetId: targetId))
            .map(ChatRoomJoinSuccessModel.self)
            .subscribe(onSuccess: {  model in
                print(model)
            })
            .disposed(by: disposeBag)
    }
    
    // 用户请求 主播同意申请
    private func agreeToSpeaker(agree: Bool, msg: V2TIMMessage)  {
        guard let index = self.requesterList.firstIndex(of: msg) else {
            return
        }
        if !agree {
            self.requesterList.remove(at: index)
            self.state.requestListPublish.accept(self.requesterList)
        } else {
            guard role == .owner || role == .manager, let targetId = msg.sender else { return }
            chatRoomProvider.rx
                .request(.pickUserSpeak(cid: eventId, targetId: targetId))
                .map(ChatRoomJoinSuccessModel.self)
                .subscribe(onSuccess: { [weak self] model in
                    print(model)
                    guard let self = self else { return }
                    self.requesterList.remove(at: index)
                    self.state.requestListPublish.accept(self.requesterList)
                })
                .disposed(by: disposeBag)
        }
    }
    
    // 踢用户下麦
    private func kickUserFromMic(targetId: String)  {
        guard role == .owner || role == .manager, !targetId.isEmpty else { return }
        chatRoomProvider.rx
            .request(.micKick(cid: eventId, targetId: targetId))
            .map(ChatRoomJoinSuccessModel.self)
            .subscribe(onSuccess: { model in
                print(model)
            })
            .disposed(by: disposeBag)
    }
    
    // 禁言某人
    private func muteSelfMic()  {
        chatRoomProvider.rx
            .request(.micMute(cid: eventId, customerId: AccountManager.shared.currentAccount?.id ?? ""))
            .mapJSON()
            .subscribe(onSuccess: { model in
                print(model)
            })
            .disposed(by: disposeBag)
    }
    
    // 解禁某人
    private func unMuteSelfMic()  {
        chatRoomProvider.rx
            .request(.micUnMute(cid: eventId, customerId: AccountManager.shared.currentAccount?.id ?? ""))
            .mapJSON()
            .subscribe(onSuccess: { model in
                print(model)
            })
            .disposed(by: disposeBag)
    }
    
    
    // 全部禁麦
    private func muteAllMic()  {
        guard role == .owner || role == .manager else { return }
        chatRoomProvider.rx
            .request(.micMuteAll(cid: eventId))
            .map(ChatRoomJoinSuccessModel.self)
            .subscribe(onSuccess: { model in
                print(model)
            })
            .disposed(by: disposeBag)
    }
    
    // 解除全部禁麦
    private func unMuteAllMic()  {
        guard role == .owner || role == .manager else { return }
        chatRoomProvider.rx
            .request(.micUnMuteAll(cid: eventId))
            .map(ChatRoomJoinSuccessModel.self)
            .subscribe(onSuccess: { model in
                print(model)
            })
            .disposed(by: disposeBag)
    }
    
    
    // 用户发消息请求上麦
    private func sendMsgToSpeaker()  {
        guard let session = self.session else { return }
        //        "{\"msg\":{\"customerId\":\"2022101407082545802006\",\"cmd\":\"1002\"},\"customMsgType\":\"MSG_PUSH\",\"businessID\":\"custom_msg_push\"}"
        let msgDetailModel = ChatRoomMsgModel(customerId: userId, cmd: ChatMessageType.REQUEST_TO_SPEAKER.rawValue)
        let messageModel = ChatRoomMessageModel.init(msg: msgDetailModel, customMsgType: "MSG_PUSH", businessID: "custom_msg_push")
        if let jsonString = messageModel.toJSONString() {
            session.sendMessage(jsonString, groupID: self.groupID) { info in
                print("\(info)")
            }
        }
    }
    
    // 群组解散
    private func groupIsDismiss()  {
        guard let session = self.session else { return }
        session.deinitAction()
        self.state.groupIsDismiss.accept(())
    }
    
    // 处理消息
    private func dealReciveMessage(_ msg: V2TIMMessage)  {
        if let custom = msg.customElem,
           let msgData = custom.data,
           let message = String(data: msgData, encoding: .utf8),
           let messageModel = ChatRoomMessageModel.deserialize(from: message),
           let msgModel = messageModel.msg {
            Logger.log(message: "\(msgModel)", level: .info)
            switch msgModel.cmd {
            case ChatMessageType.PICK_USER.rawValue:
                if msgModel.customerId == self.userId {
                    listenerSwitchMic(toSpeaker: true)
                }
                self.state.beSpeakerPublish.accept(true)
                self.getGroupMember()
            case ChatMessageType.KIKI_USER.rawValue:
                if msgModel.customerId == self.userId {
                    listenerSwitchMic(toSpeaker: false)
                    self.state.beSpeakerPublish.accept(false)
                }
                self.getGroupMember()
            case ChatMessageType.LEVE_GROUP.rawValue:
                self.getGroupMember()
            case ChatMessageType.MUTE.rawValue:
                if msgModel.customerId == self.userId {
                    beMuteMic(mute: true)
                }
                self.getGroupMember()
            case ChatMessageType.UNMUTE.rawValue:
                if msgModel.customerId == self.userId {
                    beMuteMic(mute: false)
                }
                self.getGroupMember()
            case ChatMessageType.ALL_MUTE.rawValue:
                self.state.allMuteStatus.accept(true)
                self.beMuteMic(mute: true)
                self.getGroupMember()
            case ChatMessageType.ALL_UNMUTE.rawValue:
                self.state.allMuteStatus.accept(false)
                self.beMuteMic(mute: false)
                self.getGroupMember()
            case ChatMessageType.CHANGE_GROUP_NAME.rawValue:
                if role != .owner {
                    self.getEventDetail()
                }
            case ChatMessageType.REQUEST_TO_SPEAKER.rawValue:
                if role == .owner || role == .manager {
                    if !self.requesterList.contains(msg) {
                        self.requesterList.insert(msg, at: 0)
                    }
                }
                self.state.requestListPublish.accept(self.requesterList)
            case ChatMessageType.DESTROY_GROUP.rawValue:
                if role != .owner && role != .manager {
                    self.groupIsDismiss()
                }
            default:
                break
            }
        }
    }
    
    
    // 用户上麦/下麦
    private func listenerSwitchMic(toSpeaker: Bool)  {
        guard let mediaKit = mediaKit else { return }
        mediaKit.muteLocalAudioStream(!toSpeaker)
        mediaKit.setClientRole(toSpeaker ? .broadcaster : .audience)
    }
    
    // 被禁言/ 解禁
    private func beMuteMic(mute: Bool)  {
        guard let mediaKit = mediaKit else { return }
        mediaKit.muteLocalAudioStream(mute)
    }
    
    
    // 活动修改后 获取活动详情
    private func getEventDetail()  {
        chatRoomProvider.rx
            .request(.getEventDetailInfo(eventId: eventId, needEventOwner: true))
            .map(ChatRoomInfo.self)
            .showHUD()
            .subscribe(onSuccess: { [weak self] model in
                if let title = model.title {
                    self?.state.roomTitlePublish.accept(title)
                }
            })
            .disposed(by: disposeBag)
    }
    
}

// 提供外部控制推拉流方法
extension ChatRoomViewModel {
    public func openRoomStream() {
        guard let mediaKit = mediaKit else { return }
        mediaKit.muteLocalAudioStream(false)
        mediaKit.muteAllRemoteAudioStream(false)
    }
    
    public func closeRoomStream() {
        guard let mediaKit = mediaKit else { return }
        mediaKit.muteLocalAudioStream(true)
        mediaKit.muteAllRemoteAudioStream(true)
    }
}

