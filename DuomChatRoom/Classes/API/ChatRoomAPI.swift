//
//  ChatRoomAPI.swift
//  Differentiator
//
//  Created by kuroky on 2022/9/29.
//

import Foundation
import DuomNetworkKit
import Moya


let chatRoomProvider = MoyaProvider<ChatRoomApi>(plugins: NetworkConfig.plugins)

enum ChatRoomApi {
    // 开启活动
    case startEvent(eventId: String)
    // 抱用户上麦
    case pickUserSpeak(cid: String, targetId: String)
    // 加入活动
    case joinRoom(eventId: String)
    // 结束活动
    case finishEvent(eventId: String)
    // 只支持修改标题或者话题
    case modifyRoomTitle(eventId: String, title: String)
    // 离开活动
    case leaveRoom(eventId: String)
    // 麦位上用户列表(包含头像)
    case preDetailList(cid: String)
    // 麦位上用户列表(仅禁麦标识)
    case micDetailList(cid: String)
    // 禁麦 customerid
    case micMute(cid: String, customerId: String)
    // 解除禁麦
    case micUnMute(cid: String, customerId: String)
    // 解除全员禁麦
    case micUnMuteAll(cid: String)
    // 下麦
    case micLeave(cid: String)
    // 踢用户下麦
    case micKick(cid: String, targetId: String)
    // 抱用户上麦
    case micPick(cid: String, targetId: String)
    // 全员禁麦
    case micMuteAll(cid: String)
    // 获取活动详情
    case getEventDetailInfo(eventId: String, needEventOwner: Bool)
}

extension ChatRoomApi: NetworkAPI {
    var parameters: [String : Any] {
        switch self {
        case let .preDetailList(cid: cid):
            return ["cid": cid]
        case let .micDetailList(cid: cid):
            return ["cid": cid]
        case let .modifyRoomTitle(eventId: eventId, title: title):
            return ["eventId": eventId, "title": title]
        case let .getEventDetailInfo(eventId: _, needEventOwner: needEventOwner):
            return ["needEventOwner": needEventOwner]
//        case let .micMute(cid: _, customerId: customerId):
//            return ["customerId": customerId]
//        case let .micUnMute(cid: _, customerId: customerId):
//            return ["customerId": customerId]
        default:
            return [:]
        }
    }
    
    var urlParameters: [String : Any] {
        switch self {
        case let .pickUserSpeak(cid: cid, targetId: targetId):
            return ["cid": cid, "targetCustomerId": targetId]
        case let .micKick(cid: cid, targetId: targetId):
            return ["cid": cid, "targetCustomerId": targetId]
        case let .micMute(cid: cid, customerId: _):
            return ["cid": cid]
        case let .micUnMute(cid: cid, customerId: _):
            return ["cid": cid]
        case let .micMuteAll(cid: cid):
            return ["cid": cid]
        case let .micUnMuteAll(cid: cid):
            return ["cid": cid]
        default:
            return [:]
        }
    }

    var path: String {
        switch self {
        case let .startEvent(eventId: eventId):
            return "/v1/celebrity/events/\(eventId)/start"
        case .pickUserSpeak:
            return "/v1/celebrity/mic/pick"
        case let .joinRoom(eventId: eventId):
            return "/v1/celebrity/events/\(eventId)/join"
        case let .finishEvent(eventId: eventId):
            return "/v1/celebrity/events/\(eventId)/finish"
        case .modifyRoomTitle:
            return "/v1/celebrity/events"
        case let .leaveRoom(eventId: eventID):
            return "/v1/celebrity/events/\(eventID)/leave"
        case .preDetailList:
            return "/v1/celebrity/mic/detail"
        case .micDetailList:
            return "/v1/celebrity/mic"
        case let .micMute(cid: _, customerId: customerId):
            return "/v1/celebrity/mic/mute/\(customerId)"
        case let .micUnMute(cid: _, customerId: customerId):
            return "/v1/celebrity/mic/unmute/\(customerId)"
        case .micUnMuteAll:
            return "/v1/celebrity/mic/unmute-all"
        case .micLeave:
            return "/v1/celebrity/mic/leave"
        case .micKick:
            return "/v1/celebrity/mic/kick"
        case .micPick:
            return "/v1/celebrity/mic/pick"
        case .micMuteAll:
            return "/v1/celebrity/mic/mute-all"
        case let .getEventDetailInfo(eventId: eventId, needEventOwner: _):
            return "/v1/celebrity/events/\(eventId)"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .preDetailList, .micDetailList, .getEventDetailInfo:
            return .get
        case .joinRoom, .leaveRoom, .startEvent, .micPick, .micKick, .micMute:
            return .post
        case .modifyRoomTitle:
            return .put
        default:
            return .post
        }
    }
}
