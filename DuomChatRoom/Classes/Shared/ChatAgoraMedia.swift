//
//  ChatAgoraMedia.swift
//  DuomChatRoom_Example
//
//  Created by kuroky on 2022/9/20.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import AgoraRtcKit
import DuomBase

public enum Speaker {
    case local
    case other(agoraUid: UInt)
}

extension AgoraRtcVideoCanvas {
    convenience init(streamId: UInt, view: UIView) {
        self.init()
        self.renderMode = .hidden
        self.uid = streamId
        self.view = view
    }
}

public class ChatAgoraMedia: NSObject {
    typealias JoinChannelCompletion = ((UInt) -> Void)?
    
   
    private var activeSpeaker = PublishRelay<Speaker>()
    var activeSpeakerObservable: Observable<Speaker> {
        return activeSpeaker.asObservable()
    }
    
    private var agoraOptions = AgoraRtcChannelMediaOptions()
    
    var agoraKit: AgoraRtcEngineKit?
    
    var role: ChatRoomRole = .listener
    
    public override init() {
        super.init()
    }
    
    public convenience init(role: ChatRoomRole) {
        self.init()
        self.role = role
        agoraKit = AgoraRtcEngineKit.sharedEngine(withAppId: CRConfig.default().AgoraAppId, delegate: self)
        agoraKit?.setChannelProfile(.liveBroadcasting)
        agoraKit?.setClientRole(role == .owner ? .broadcaster : .audience)
        agoraKit?.enableAudio()
        agoraKit?.disableVideo()
        agoraKit?.enableAudioVolumeIndication(1000, smooth: 3, reportVad: false)
    }
    
    deinit {
         Logger.log(message: "ChatAgoraMedia deinit", level: .info)
    }
    
}

// MARK: functions
extension ChatAgoraMedia {
    func joinChannel(_ token: String, channelId: String, userID: String, completion: JoinChannelCompletion = nil)  {
        guard let agoraKit = agoraKit else { return }
        agoraKit.muteAllRemoteAudioStreams(false)
//        let a = agoraKit.joinChannel(byToken: token, channelId: channelId, info: nil, uid: UInt("111")!) { s1, uid, s2 in
//            Logger.log(message: "join channel success", level: .info)
//            if let joinSuccess = completion {
//                joinSuccess(uid)
//            }
//        }
        
        agoraKit.joinChannel(byToken: token, channelId: channelId, userAccount: userID) { s1, uid, s2 in
            Logger.log(message: "join channel success", level: .info)
            if let joinSuccess = completion {
                joinSuccess(uid)
            }
        }
    }
    
    func leaveChannel()  {
        guard let agoraKit = agoraKit else { return }
        agoraKit.leaveChannel()
        agoraKit.muteLocalAudioStream(true)
    }
    
    func muteLocalAudioStream(_ mute: Bool)  {
        guard let agoraKit = agoraKit else { return }
        agoraKit.muteLocalAudioStream(mute)
    }
    
    func muteAllRemoteAudioStream(_ mute: Bool)  {
        guard let agoraKit = agoraKit else { return }
        agoraKit.muteAllRemoteAudioStreams(mute)
    }
    
    func setClientRole(_ role: AgoraClientRole) {
        guard let agoraKit = agoraKit else { return }
        agoraKit.setClientRole(role)
    }
    
    func deinitAction()  {
        agoraKit?.leaveChannel()
        AgoraRtcEngineKit.destroy()
    }
}

extension ChatAgoraMedia: AgoraRtcEngineDelegate {
    public func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinChannel channel: String, withUid uid: UInt, elapsed: Int) {
        Logger.log(message: "local join success: \(uid)", level: .info)
    }
    public func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
        Logger.log(message: "remote join success: \(uid)", level: .info)
    }
    
    public func rtcEngine(_ engine: AgoraRtcEngineKit, reportAudioVolumeIndicationOfSpeakers speakers: [AgoraRtcAudioVolumeInfo], totalVolume: Int) {
        if totalVolume > 30 {
            for user in speakers {
                let speakerUid = user.uid
                // 目前只监听远端的
                if speakerUid != 0 {
                    activeSpeaker.accept(.other(agoraUid: speakerUid))
                }
                
//                if speakerUid == 0 {
////                    activeSpeaker.accept(.local)
//                } else {
//                    activeSpeaker.accept(.other(agoraUid: speakerUid))
//                }
            }
        }
    }
}
