//
//  ChatSessison.swift
//  DuomChatRoom_Example
//
//  Created by kuroky on 2022/9/20.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import DuomBase
import KKIMModule

public class ChatSessison: NSObject {
    typealias SuccessCompletion = ((String) -> Void)?
    typealias FailCompletion = ((Int32, String) -> Void)?
    typealias MemberListSuccessCompletion = ((Int, [ChatRoomMemberModel]) -> Void)?
    
    private var imHelper: DuomHelper = DuomHelper.sharedInstance()
    
    var role: ChatRoomRole? = .listener
    
    private var newmessage: BehaviorRelay<V2TIMMessage?> = BehaviorRelay<V2TIMMessage?>(value: nil)
    var newmessageObservable: Observable<V2TIMMessage?> {
        return newmessage.asObservable()
    }
    
    private var updateListPublish: PublishRelay<Void> = PublishRelay<Void>()
    var updateListObservable: Observable<Void> {
        return updateListPublish.asObservable()
    }
    
    public override init() {
        super.init()
    }
    
    public convenience init(role: ChatRoomRole?) {
        self.init()
        imHelper.delegate = self
        addGroupListener()
        addGroupMsgListener()
    }
    
    deinit {
        Logger.log(message: "ChatSessison deinit", level: .info)
        removeGroupListener()
        removeGroupMsgListener()
    }
}

// MARK: functions
extension ChatSessison {
    func createGroup(_ groupID: String, groupName: String, successCompletion: SuccessCompletion, failCompletion: FailCompletion = nil)  {
        imHelper.createGroup(groupID, groupName: groupName, groupType: .DUOM_GROUP_COMMUNITY_TYPE) { [weak self] in
            Logger.log(message: "create group success", level: .info)
            self?.addGroupListener()
            self?.addGroupMsgListener()
            if let success = successCompletion {
                success($0)
            }
        } fail: { [weak self] code, desc in
            Logger.log(message: "create group failure: \(code) \(desc)", level: .error)
            if code == 10025 {
                self?.addGroupListener()
                self?.addGroupMsgListener()
                if let success = successCompletion {
                    success("")
                }
            } else {
                if let fail = failCompletion {
                    fail(code, desc)
                }
            }
        }
    }
    
    func joinGroup(_ groupID: String, successCompletion: SuccessCompletion = nil, failCompletion: FailCompletion = nil)  {
        imHelper.joinGroup(groupID) { [weak self] in
            Logger.log(message: "join group success", level: .info)
            self?.addGroupListener()
            self?.addGroupMsgListener()
            if let success = successCompletion {
                success("")
            }
        } fail: { code, desc in
            Logger.log(message: "join group failure: \(code) \(desc)", level: .error)
            if let fail = failCompletion {
                fail(code, desc)
            }
        }
    }
    
    public func addGroupListener()  {
        imHelper.addGroupListener()
    }
    
    public func removeGroupListener()  {
        imHelper.removeGroupListener()
    }
    
    public func addGroupMsgListener()  {
        imHelper.addAdvancedMsgListener()
    }
    
    public func removeGroupMsgListener()  {
        imHelper.removeAdvancedMsgListener()
    }
    
    func quitGroupAction(_ groupID: String, successCompletion: SuccessCompletion = nil, failCompletion: FailCompletion = nil) {
        if role == .owner {
            dismissGroup(groupID, successCompletion: successCompletion, failCompletion: failCompletion)
        } else {
            quitFromGroup(groupID, successCompletion: successCompletion, failCompletion: failCompletion)
        }
    }
    
    private func dismissGroup(_ groupID: String, successCompletion: SuccessCompletion, failCompletion: FailCompletion)  {
        imHelper.dismissGroup(groupID) { [weak self] in
            guard let self = self else { return }
            Logger.log(message: "群组解散成功", level: .info)
            self.removeGroupMsgListener()
            if let success = successCompletion {
                success("")
            }
        } fail: { code, desc in
            Logger.log(message: "群组解散失败", level: .warning)
            if let fail = failCompletion {
                fail(code, desc)
            }
        }
    }
    
    private func quitFromGroup(_ groupID: String, successCompletion: SuccessCompletion, failCompletion: FailCompletion)  {
        imHelper.quitGroup(groupID) { [weak self] in
            guard let self = self else { return }
            Logger.log(message: "退出群组成功", level: .info)
            self.removeGroupMsgListener()
            self.removeGroupMsgListener()
            if let success = successCompletion {
                success("")
            }
        } fail: { code, desc in
            Logger.log(message: "退出群组失败: \(desc)", level: .warning)
            if let fail = failCompletion {
                fail(code, desc)
            }
        }
    }
    
    func sendMessage(_ msg: String, groupID: String, successCompletion: SuccessCompletion, failCompletion: FailCompletion = nil)  {
        let data: Data = msg.data(using: .utf8)!
        imHelper.sendGroupCustomMessage(data, groupID: groupID, priority: DuomIMMessagePriority.DUOM_IM_PRIORITY_NORMAL) {
            Logger.log(message: "send msg success", level: .info)
            if let success = successCompletion {
                success("")
            }
        } fail: { code, desc in
            Logger.log(message: "send msg failure: \(desc)", level: .error)
            if let fail = failCompletion {
                fail(code, desc)
            }
        }
    }
    
    func getGroupMemberList(_ groupID: String, page: Int, successCompletion: MemberListSuccessCompletion, failComletion: FailCompletion = nil)  {
        imHelper.getGroupMemberList(groupID, page: Int32(page)) { nextPage, list in
            var models = [ChatRoomMemberModel]()
            let sortList = list.sorted(by: { $0.role > $1.role })
            sortList.forEach {
                var tempRole: ChatRoomRole = .listener
                if $0.role == 200 {
                    tempRole = .listener
                } else if $0.role == 300 {
                    tempRole = .speaker
                } else if $0.role == 400 {
                    tempRole = .owner
                }

                let tempModel = ChatRoomMemberModel.init(role: tempRole, groupMute: false, micStatus: .connect, customerId: $0.userID, name: $0.nickName, avatar: $0.faceURL)
                models.append(tempModel)
            }
            if let success = successCompletion {
                success(Int(nextPage), models)
            }
        } fail: { code, desc in
            if let failComletion = failComletion {
                failComletion(code, desc)
            }
        }
    }
    
    func deinitAction()  {
        self.removeGroupMsgListener()
        self.removeGroupListener()
    }
    
}

extension ChatSessison: DuomIMDelegate {
    public func onConnect(_ stateType: DuomIMStateType) {
        if stateType == .DUOM_IM_STATE_KICKEDOFFLINE_TYPE
            || stateType == .DUOM_IM_STATE_USERSIGEXPIRED_TYPE
            || stateType == .DUOM_IM_STATE_CONNECTFAIL_TYPE {
            toast("Group State Error")
        }
    }
    
    public func onRecvNewMessage(_ msg: V2TIMMessage) {
        newmessage.accept(msg)
    }
    
    public func onMemberEnter(_ groupID: String, memberList: [V2TIMGroupMemberInfo]) {
        Logger.log(message: "new member: \(memberList)", level: .info)
        self.updateListPublish.accept(())
    }
    
    public func onMemberLeave(_ groupID: String, member: V2TIMGroupMemberInfo) {
        Logger.log(message: "member leave: \(member)", level: .info)
        self.updateListPublish.accept(())
    }
    
    public func onReceiveRESTCustomData(_ groupID: String, data: Data) {
        Logger.log(message: "none action", level: .info)
    }
    
    public func onGroupDismissed(_ groupID: String, opUser: V2TIMGroupMemberInfo) {
        Logger.log(message: "none action", level: .info)
    }
    
    public func onMemberInfoChanged(_ groupID: String, changeInfoList: [V2TIMGroupMemberChangeInfo]) {
        Logger.log(message: "none action", level: .info)
    }
    
    public func onGroupInfoChanged(_ groupID: String, changeInfoList: [V2TIMGroupChangeInfo]) {
        Logger.log(message: "onGroup InfoChanged", level: .info)
    }
    
    public func onGrantAdministrator(_ groupID: String, opUser: V2TIMGroupMemberInfo, memberList: [V2TIMGroupMemberInfo]) {
        Logger.log(message: "none action", level: .info)
    }
    
    public func onRevokeAdministrator(_ groupID: String, opUser: V2TIMGroupMemberInfo, memberList: [V2TIMGroupMemberInfo]) {
        Logger.log(message: "none action", level: .info)
    }
    
    
}
