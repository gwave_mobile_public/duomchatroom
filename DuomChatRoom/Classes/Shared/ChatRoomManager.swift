//
//  ChatRoomManager.swift
//  DuomChatRoom
//
//  Created by kuroky on 2022/10/17.
//

import Foundation
import DuomBase
import RxSwift
import RxCocoa

public class ChatRoomManager {
    static let disposeBag = DisposeBag()
    // 打开chatRoom
    public static func openLiveRoom(eventId: String, role: ChatRoomRole, roomName: String?, roomTopic: String?, parentNavigator: UINavigationController?)  {
        guard let topVC = UIApplication.topViewController() else { return  }
        // 校验权限
        AuthorizationHelper.checkAuthorizationWithAlert(option: [.recordPermission], from: topVC)
            .andThen(Observable.just(true))
            .subscribe(onNext: { _ in
                let chatRoomVC = ChatRoomMainViewController().then {
                    $0.viewModel = ChatRoomViewModel(role: role,
                                                          eventId: eventId,
                                                          userId: AccountManager.shared.currentAccount?.id ?? "",
                                                          roomName: roomName,
                                                          roomTopic: roomTopic)
                }
                parentNavigator?.present(chatRoomVC, animated: true)
            }, onError: { _ in
                toast(CRConfig.default().authorzationTip)
            })
            .disposed(by: disposeBag)
        
    }
    
    // MARK: 流控制
    public static func openRoomStream() {
        CRSharedStreamWindow.shared().openRoomStream()
    }
    
    public static func closeRoomStream() {
        CRSharedStreamWindow.shared().closeRoomStream()
    }
    
    public static func closeMiniRoomWindow() {
        CRSharedStreamWindow.destroy()
    }
}

