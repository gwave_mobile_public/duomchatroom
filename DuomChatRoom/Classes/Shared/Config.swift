//
//  Config.swift
//  DuomChatRoom
//
//  Created by kuroky on 2022/9/20.
//

import UIKit

public final class CRConfig {
    private static var single = CRConfig()
    
    public class func `default`() -> CRConfig {
        return CRConfig.single
    }
    
    // Agora 
    public let AgoraAppId: String = "7c939bd01ad54984978ad725c58e2db1"
    
    /**
     业务配置
     */
    /// 管理员个数
    public let coHostMaxCount = 3
    /// 最多同时在线speaker个数
    public let speakersMaxCount = 17
    
    /**
     房间角色
     */
    /// 房主
    public let hostRole: String = "Host"
    /// 管理员
    public let coHostRole: String = "Co-Host"
    /// speaker
    public let speakerRole: String = "Speaker"
    /// 听众
    public let listenerRole: String = "Listener"
    
    /**
     文案
     */
    /// 权限校验失败
    public let authorzationTip: String = "You do not have permission to use this feature."
    
    /// 进入房间时的提示文案
    public let enterTip: String = "The host is recording this chat room. Everyone that speaks will be included in the public recording."
    
    /// 离开房间 alert title
    public let leaveRoomTitle: String = "Leave Chat Room"

    /// 离开房间 alert message
    public let leaveRoomMessage: String = "Do you want to leave this chat room?"
    
    /// 结束房间 alert title
    public let endRoomTitle: String = "End Chat Room"
    
    /// 结束房间 主播主动结束 alert message
    public let endRoomAuthorMessage: String = "Do you want to end this chat Room?"
    
    /// 结束房间 alert title
    public let endRoomAudienceMessage: String = "This chat room has ended."
    
    /// 用户申请上麦时的提示title
    public let requestToSpeakerTitle: String = "People might be able to listen to this chat room after it’s over"
    
    /// 用户申请上麦时的提示详细信息
    public let requestToSpeakaderDetail: String = "The host might save this recording. Your voice could be in a replau and heard by more people after this chat room ends."
    
    /**
     对用户操作
     */
    /// 设置成speaker
    public let toSpeakerAction: String = "Set to Speaker"
    
    /// 移除speaker
    public let removeSpeakerAction: String = "Remove to Speaker"
    
    /// 禁言
    public let muteUserAction: String = "Mute This User"
    
    /// 解禁
    public let unMuteUserAction: String = "unMute This User"
    
   
    // lottieSource资源读取
    public var getCRLottieSource: (String, String) -> URL? = { sourceName, sourceExt in
        return Bundle(url: Bundle.main.url(forResource: "lottieCRSource", withExtension: "bundle")!)!.url(forResource: sourceName, withExtension: sourceExt)
    }
    
    public var getCRLottieBundle = Bundle(url: Bundle.main.url(forResource: "lottieCRSource", withExtension: "bundle")!)!
}



// MARK: chat命名空间
public struct ChatRoomWrapper<Base> {
    public let base: Base
    
    init(_ base: Base) {
        self.base = base
    }
}

public protocol ChatRoomCompatible: AnyObject {}

public protocol ChatRoomCompatibleValue {}

extension ChatRoomCompatible {
    public var chat: ChatRoomWrapper<Self> {
        get {
            ChatRoomWrapper(self)
        }
        set {}
    }
    
    public static var chat: ChatRoomWrapper<Self>.Type {
        get {
            ChatRoomWrapper<Self>.self
        }
        set {}
    }
}

extension ChatRoomCompatibleValue {
    public var chat: ChatRoomWrapper<Self> {
        get {
            ChatRoomWrapper(self)
        }
        set {}
    }
}

extension UIViewController: ChatRoomCompatible { }
extension UIColor: ChatRoomCompatible { }
extension UIImage: ChatRoomCompatible { }
extension UIView: ChatRoomCompatible { }

extension Array: ChatRoomCompatibleValue { }
extension String: ChatRoomCompatibleValue { }
extension CGFloat: ChatRoomCompatibleValue { }
extension Bool: ChatRoomCompatibleValue { }
