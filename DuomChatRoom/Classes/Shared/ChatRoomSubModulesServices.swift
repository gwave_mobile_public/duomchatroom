//
//  ChatRoomSubModulesServices.swift
//  DuomChatRoom
//
//  Created by kuroky on 2022/12/16.
//  VC解偶
//

import Foundation
import DuomBase

private var lastChatRoomSubModuleRec = NSMapTable<NSString, AnyObject>.strongToWeakObjects()
private var lastChatRoomViewModel: ChatRoomViewModel?

private extension ViewControllerSubModule {
    func findChatRoomRepository<T>(_ type: T.Type) -> T? {
        let recKey = String(describing: type) as NSString
        if let repository = subModuleService.repository as? T {
            lastChatRoomSubModuleRec.setObject(repository as AnyObject, forKey: recKey)
            return repository
        }
        return lastChatRoomSubModuleRec.object(forKey: recKey) as? T
    }
}

public protocol ChatRoomSubModule: ViewControllerSubModule, ViewType {
    func getChatRoomViewModel()
}

extension ChatRoomSubModule where Self: UIViewController {
    var mediaKit: ChatAgoraMedia? {
        return (viewModel as? ChatRoomViewModel)?.mediaKit
    }
    
    var session: ChatSessison? {
        return (viewModel as? ChatRoomViewModel)?.session
    }
    
    // 转成具体的业务vm
    public func getChatRoomViewModel()  {
        if let repository = findChatRoomRepository(ChatRoomViewModel.self) {
            lastChatRoomViewModel = repository
            if let targetViewModel = repository as? TargetViewModel {
                self.viewModel = targetViewModel
            }
            return
        }
        
        if let lastVM = lastChatRoomViewModel {
            if let targetViewModel = lastVM as? TargetViewModel {
                self.viewModel = targetViewModel
            }
            return
        }
       
        fatalError("not find TargetViewModel(ChatRoomViewModel) in ViewController Chain! \(self)")
    }
}
