//
//  GustsListTableViewCell.swift
//  Duom
//
//  Created by kuroky on 2022/9/16.
//

import UIKit
import DuomBase
import RxSwift

class GuestsListTableViewCell: BaseTableViewCell, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(GuestsListTableViewCell.self)
    
    private let headerView = RoundImageView(backgroundColor: .themeColor).then {
        $0.contentMode = .scaleAspectFill
    }
    
    private let nameLabel = UILabel().then {
        $0.textColor = UIColor.textWhiteColor
        $0.font = UIFont.appFont(ofSize: 16, fontType: .Inter_Blod)
        $0.textAlignment = .left
    }
    
    private let muteButton = UIButton().then {
        $0.setImage(UIImage(named: "mic_off_white"), for: .normal)
        $0.setImage(UIImage(named: "mic_off_small"), for: .selected)
        $0.isUserInteractionEnabled = false
    }
    
    private let moreButton = UIButton().then {
        $0.setImage(UIImage(named: "more_icon"), for: .normal)
    }
    
    private let disposeBag = DisposeBag()
    
    private(set) var role: ChatRoomRole?
    
    private(set) var type: MemberRoleType?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = UIColor.themeBlackColor
        contentView.addSubview(headerView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(muteButton)
        contentView.addSubview(moreButton)
        
        headerView.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.size.equalTo(CGSize(width: 55, height: 55))
            make.top.equalTo(8)
            make.bottom.equalTo(-8)
        }
        
        nameLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(headerView.snp.trailing).offset(12)
            make.height.equalTo(22.DScale)
        }
        
        muteButton.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.size.equalTo(CGSize(width: 32, height: 32).DScale)
            make.trailing.equalTo(moreButton.snp.leading).offset(-16.DScale)
        }
        
        moreButton.snp.makeConstraints { make in
            make.trailing.equalTo(-18.DScale)
            make.centerY.equalToSuperview()
            make.size.equalTo(muteButton.snp.size)
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    // bindViewModel
    func bindViewModel()  {
        
    }
    
    override func addUIActions() {
        moreButton.addTarget(self, action: #selector(addMoreAction(_:)), for: .touchUpInside)
    }
    
    @objc func addMoreAction(_ sender: UIButton) {
        guard let role = role, let type = type else { return }

        if role != .owner, role != .manager {
            return
        }
        
        switch type {
        case .host:
            break
        case .co_host:
            if role == .owner {
                self.showHandleHosterAlert()
            }
        case .speaker:
            self.showHandleSpeakerAlert()
        case .listener:
            self.showHandleListenerAlert()
        case .all:
            break
        }
    }

    func configCell(role: ChatRoomRole, type: MemberRoleType, mode: ChatRoomMemberModel)  {
        self.role = role
        self.type = type
        muteButton.isHidden = (role != .owner && role != .manager)
        moreButton.isHidden = muteButton.isHidden
        
        nameLabel.text = mode.nickName
        if let avatar = mode.avatar, avatar.isNotEmpty {
            headerView.kf.setImage(with: URL(string: avatar))
        }
        
        switch type {
        case .host:
            break
        case .co_host, .speaker:
            muteButton.isHidden = (role != .owner && role != .manager)
            moreButton.isHidden = muteButton.isHidden
            muteButton.isSelected = mode.micStatus == .disConnect
            muteButton.isSelected = mode.groupMute
        case .listener:
            muteButton.isHidden = true
            moreButton.isHidden = (role != .owner && role != .manager)
        case .all:
            break
        }
    }
    
    
    private func showHandleHosterAlert()  {
        let vc = OwnerActionSheetViewController(type: .toManager, eventSource1: {
            print("source1")
        }, eventSource2: {
            print("source2")
        })
        UIApplication.topViewController()?.present(vc, animated: true)
    }
    
    private func showHandleSpeakerAlert()  {
        let vc = OwnerActionSheetViewController(type: .toSpeaker, eventSource1: {
            print("source1")
        }, eventSource2: {
            print("source2")
        })
        UIApplication.topViewController()?.present(vc, animated: true)
    }
    
    private func showHandleListenerAlert()  {
        let vc = OwnerActionSheetViewController(type: .toListener, eventSource1: {
            print("source1")
        }, eventSource2: {
            print("source2")
        })
        UIApplication.topViewController()?.present(vc, animated: true)
    }
}
