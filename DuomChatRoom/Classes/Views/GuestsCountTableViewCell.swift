//
//  GuestsCountTableViewCell.swift
//  Duom
//
//  Created by kuroky on 2022/9/16.
//

import UIKit
import DuomBase

class GuestsCountTableViewCell: BaseTableViewCell, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(GuestsCountTableViewCell.self)
    
    var muteAction: ((Bool)->Void)?
    
    private let headerLabel = UILabel().then {
        $0.textColor = UIColor.textWhiteColor
        $0.font = .appFont(ofSize: 20, fontType: .GT)
        $0.textAlignment = .left
    }
    
    private let numberLabel = UILabel().then {
        $0.textColor = UIColor.textLightColor
        $0.font = .appFont(ofSize: 12, fontType: .Inter_Medium)
        $0.textAlignment = .left
    }
    
    private let muteButton = UIButton().then {
        $0.setTitle("Mute all", for: .normal)
        $0.setTitle("unMute all", for: .selected)
        $0.setTitleColor(UIColor.themeColor, for: .normal)
        $0.titleLabel?.textAlignment = .center
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = UIColor.themeBlackColor
        contentView.addSubview(headerLabel)
        contentView.addSubview(numberLabel)
        contentView.addSubview(muteButton)
        
        headerLabel.snp.makeConstraints { make in
            make.top.equalTo(24)
            make.leading.equalTo(16.DScale)
            make.height.equalTo(24)
        }
        
        numberLabel.snp.makeConstraints { make in
            make.leading.equalTo(headerLabel.snp.leading)
            make.top.equalTo(headerLabel.snp.bottom).offset(8)
            make.height.equalTo(15)
            make.bottom.equalTo(-4)
        }
        
        muteButton.snp.makeConstraints { make in
            make.trailing.equalTo(-16.DScale)
            make.centerY.equalTo(headerLabel.snp.centerY)
            make.size.equalTo(CGSize(width: 80, height: 20).DScale)
        }
        
        muteButton.addTarget(self, action: #selector(muteAllAction(_:)), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    @objc func muteAllAction(_ sender: UIButton)  {
        sender.isSelected.toggle()
        if let mute = muteAction {
            mute(sender.isSelected)
        }
    }
    
    func configCell(role: ChatRoomRole, groupMute: Bool = false, type: MemberRoleType, count: Int, leftCount: Int?)  {
        muteButton.isHidden = (role != .owner && role != .manager)
        muteButton.isSelected = groupMute
        switch type {
        case .host:
            break
        case .co_host:
            headerLabel.text = "Co-hosts"
            numberLabel.text = "\(count) Co-hosts・\(leftCount ?? 0) Open spots"
            muteButton.isHidden = true
        case .speaker:
            headerLabel.text = "Speakers"
            numberLabel.text = "\(count) Speakers・\(leftCount ?? 0) Open spots"
        case .listener:
            headerLabel.text = "Listeners"
            numberLabel.text = "\(count) people on DUOM are listening"
            muteButton.isHidden = true
        case .all:
            break
        }
    }
}
