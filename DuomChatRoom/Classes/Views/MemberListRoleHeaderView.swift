//
//  MemberListRoleHeaderView.swift
//  DuomChatRoom
//
//  Created by kuroky on 2022/12/28.
//

import Foundation
import UIKit
import DuomBase

class MemberListRoleHeaderView: UICollectionReusableView, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(MemberListRoleHeaderView.self)
    
    private let roleLabel: UILabel = UILabel().then {
        $0.textColor = .white
        $0.font = .appFont(ofSize: 20, fontType: .GT)
        $0.textAlignment = .left
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initUI()
    }
    
    func initUI()  {
        addSubview(roleLabel)
        roleLabel.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.bottom.equalTo(-6)
            make.top.equalTo(15)
            make.height.equalTo(24)
        }
    }
    
    func configType(_ role: String)  {
        roleLabel.text = role
    }
}
