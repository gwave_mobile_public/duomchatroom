//
//  RquestListCell.swift
//  Duom
//
//  Created by kuroky on 2022/9/16.
//

import UIKit
import DuomBase
import KKIMModule

class RequestListCell: BaseTableViewCell, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(RequestListCell.self)
    
    var agreeAction: ((V2TIMMessage)->Void)?
    
    var disAgreeAction: ((V2TIMMessage)->Void)?
    
    private var msgModel = V2TIMMessage()
    
    private let headerView = RoundImageView().then {
        $0.contentMode = .scaleAspectFill
        $0.image = UIImage(named: "user_placeholder")
    }
    
    private let nameLabel = UILabel().then {
        $0.textColor = UIColor.textWhiteColor
        $0.font = UIFont.appFont(ofSize: 16, fontType: .Inter_Blod)
        $0.textAlignment = .left
    }
    
    private let detailLabel = UILabel().then {
        $0.textColor = UIColor.themeColor
        $0.font = UIFont.appFont(ofSize: 16, fontType: .Inter_Blod)
        $0.textAlignment = .left
        $0.text = "Request to Speaker"
    }
    
    private let agreeButton = UIButton().then {
        $0.setImage(UIImage(named: "chatroom_agree"), for: .normal)
    }
    
    private let disAgreeButton = UIButton().then {
        $0.setImage(UIImage(named: "chatroom_disagree"), for: .normal)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = UIColor.themeBlackColor
        contentView.addSubview(headerView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(detailLabel)
        contentView.addSubview(agreeButton)
        contentView.addSubview(disAgreeButton)
        
        headerView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(16.DScale)
            make.size.equalTo(CGSize(width: 55, height: 55).DScale.toCeil)
            make.top.equalTo(8.DScale.toCeil)
            make.bottom.equalTo(-8.DScale.toCeil)
        }
        
        nameLabel.snp.makeConstraints { make in
            make.top.equalTo(headerView.snp.top).offset(4.DScale)
            make.leading.equalTo(headerView.snp.trailing).offset(12.DScale)
            make.height.equalTo(22.DScale)
            make.trailing.equalTo(agreeButton.snp.leading).offset(-30)
        }
        
        detailLabel.snp.makeConstraints { make in
            make.leading.equalTo(nameLabel.snp.leading)
            make.height.equalTo(22.DScale)
            make.top.equalTo(nameLabel.snp.bottom).offset(1)
        }
        
        agreeButton.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.size.equalTo(CGSize(width: 32, height: 32).DScale)
            make.trailing.equalTo(disAgreeButton.snp.leading).offset(-16.DScale)
        }
        
        disAgreeButton.snp.makeConstraints { make in
            make.trailing.equalTo(-18.DScale)
            make.centerY.equalToSuperview()
            make.size.equalTo(agreeButton.snp.size)
        }
        
        agreeButton.addTarget(self, action: #selector(agreeRequestAction(_:)), for: .touchUpInside)
        disAgreeButton.addTarget(self, action: #selector(disAgreeRequestAction(_:)), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func configCell(_ model: V2TIMMessage)  {
        self.msgModel = model
        if let nickName = model.nickName {
            nameLabel.text = nickName
        }
        
        if let avatar = model.faceURL {
            headerView.kf.setImage(with: URL(string: avatar))
        }
    }
    
    @objc func agreeRequestAction(_ sender: UIButton)  {
        if let agree = agreeAction {
            agree(msgModel)
        }
    }
    
    @objc func disAgreeRequestAction(_ sender: UIButton)  {
        if let disAgree = disAgreeAction {
            disAgree(msgModel)
        }
    }
}
