//
//  MemberCollectionViewCell.swift
//  Duom
//
//  Created by kuroky on 2022/9/13.
//

import UIKit
import Foundation
import DuomBase
import Kingfisher

class MemberCollectionViewCell: UICollectionViewCell, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(MemberCollectionViewCell.self)
    
    private var headerView: RoundImageView = RoundImageView().then {
        $0.image = UIImage(named: "user_placeholder")
        $0.contentMode = .scaleAspectFill
    }
    
    private var nameLabel: UILabel = UILabel().then {
        $0.textAlignment = .center
        $0.textColor = UIColor.textWhiteColor
        $0.font = UIFont.appFont(ofSize: 14, fontType: .Inter_Blod)
    }
    
    private var micOffImg: UIImageView = UIImageView().then {
        $0.backgroundColor = .black
        $0.roundCorners(radius: 20.DScale / 2)
        $0.image = UIImage(named: "mic_off_small")
    }
    
    private var talkingImg: UIImageView = UIImageView().then {
        $0.backgroundColor = UIColor(0, 0, 0, 0.5)
        $0.image = UIImage(named: "mic_wave")
        $0.isHidden = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initUI()
    }
    
    func initUI()  {
        contentView.addSubview(headerView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(micOffImg)
        headerView.addSubview(talkingImg)
        
        headerView.roundCorners(radius: self.bounds.width / 2)
        headerView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview()
            make.size.equalTo(CGSize(width: self.bounds.width, height: self.bounds.width))
        }
        
        nameLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(self.headerView.snp.bottom).offset(8.DScale)
            make.height.equalTo(17.DScale)
        }
        
        micOffImg.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 20, height: 20).DScale)
            make.bottom.equalTo(headerView.snp.bottom)
            make.trailing.equalTo(headerView.snp.trailing)
        }
        
        talkingImg.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    func configWithModel(_ model: ChatRoomMemberModel)  {
        if let name = model.nickName, name.isNotEmpty {
            nameLabel.text = model.nickName
        } else {
            nameLabel.text = ""
        }
        
        
        if let avatar = model.avatar, avatar.isNotEmpty {
            headerView.kf.setImage(with: URL(string: avatar), placeholder: UIImage(named: "user_placeholder"))
        } else {
            headerView.image = nil
        }
        
        if model.role == .listener {
            micOffImg.isHidden = true
        } else {
            if model.groupMute {
                micOffImg.isHidden = false
            } else {
                micOffImg.isHidden = model.micStatus != .disConnect
            }
        }
    }
    
//    func startTalking() {
//        print("startTalkingstartTalkingstartTalking")
//    }
}


class MemberRoleView: UIView {
    var iconImg: UIImageView = UIImageView().then {
        $0.image = UIImage(named: "coll_mic_off")
    }
    
    var roleLabel: UILabel = UILabel().then {
        $0.textColor = UIColor.textWhiteColor
        $0.textAlignment = .center
        $0.font = UIFont.appFont(ofSize: 14, fontType: .Inter_Medium)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initUI()
    }
    
    private func initUI()  {
        addSubview(iconImg)
        addSubview(roleLabel)
        iconImg.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 10.DScale, height: 13.DScale))
            make.leading.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        roleLabel.snp.makeConstraints { make in
            make.leading.equalTo(iconImg.snp.trailing).offset(6)
            make.trailing.equalToSuperview()
            make.height.equalToSuperview()
        }
    }
    
    func configWithModel(_ model: ChatRoomMemberModel)  {
        iconImg.isHidden = model.role == .listener
        updateSubViews(iconImg.isHidden)
        
        if model.groupMute {
            iconImg.image = UIImage(named: "coll_mic_off")
        } else {
            iconImg.image = UIImage(named: model.micStatus == .disConnect ?  "coll_mic_off" : "mic_wave")
        }
        
        switch model.role {
        case .owner:
            roleLabel.text = "Host"
        case .manager:
            roleLabel.text = "co-Host"
        case .speaker:
            roleLabel.text = "Speaker"
        case .listener:
            roleLabel.text = "Listener"
        }
    }
    
    private func updateSubViews(_ hidden: Bool)  {
        if hidden {
            roleLabel.snp.remakeConstraints { make in
                make.edges.equalToSuperview()
            }
        } else {
            roleLabel.snp.remakeConstraints { make in
                make.leading.equalTo(iconImg.snp.trailing).offset(6)
                make.trailing.equalToSuperview()
                make.height.equalToSuperview()
            }
        }
        
    }
    
}
