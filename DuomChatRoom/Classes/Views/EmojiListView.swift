//
//  EmojiListView.swift
//  Duom
//
//  Created by kuroky on 2022/9/16.
//

import UIKit
import DuomBase
import RxCocoa
import RxSwift
import RxDataSources

class EmojiListView: BaseView {
    private let disposeBag = DisposeBag()
    
    private let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout().then {
        let width = 30
        $0.itemSize = CGSize(width: width, height: width )
        $0.minimumInteritemSpacing = 12
        $0.scrollDirection = .horizontal
        $0.sectionInset = UIEdgeInsets(top: 6, left: 12, bottom: 6, right: 12)
    }
    
    private let collectionView = UICollectionView.init(frame: .zero, collectionViewLayout: UICollectionViewLayout()).then {
        $0.backgroundColor = UIColor(44, 43 , 43)
        $0.register(EmojiListCell.self, forCellWithReuseIdentifier: EmojiListCell.identifier)
        $0.showsHorizontalScrollIndicator = false
        $0.showsHorizontalScrollIndicator = false
    }
    
    private let dataSource = RxCollectionViewSectionedReloadDataSource<SectionModel<String, String>>.init(configureCell: {
        (datasource, collect, idx , str) -> UICollectionViewCell in
        let cell = collect.dequeueReusableCell(withReuseIdentifier: EmojiListCell.identifier, for: idx) as! EmojiListCell
        return cell
    })
    
    let items = Observable.just([
                SectionModel(model: "test", items: [
                    "UILable",
                    ])
                ])

    override func setupUI() {
        super.setupUI()
        collectionView.collectionViewLayout = layout
        self.roundCorners(radius: 5)
        addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    override func addActions() {
        items.bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        collectionView.rx.itemSelected.subscribe(onNext: {
            print("\($0)")
        })
        .disposed(by: disposeBag)
    }
   

}


class EmojiListCell: UICollectionViewCell, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(EmojiListCell.self)
    
    private let imageView = UIImageView(backgroundColor: .clear)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initUI()
    }
    
    func initUI()  {
        contentView.backgroundColor = .green
        contentView.addSubview(imageView)
        imageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
