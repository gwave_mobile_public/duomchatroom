//
//  ChatRoomEnterTipView.swift
//  Duom
//
//  Created by kuroky on 2022/9/14.
//

import UIKit
import DuomBase

class ChatRoomEnterTipView: BaseView {
    var startAction: (()->Void)?
    
    private var infoLabel: UILabel = UILabel().then {
        $0.textColor = UIColor.grayColor
        $0.font = UIFont.appFont(ofScaleBase: 12)
        $0.numberOfLines = 2
        $0.lineBreakMode = .byWordWrapping
        $0.textAlignment = .center
        $0.text = CRConfig.default().enterTip
    }
    
    private var startButton: RoundButton = RoundButton().then {
        $0.backgroundColor = UIColor.themeColor
        $0.setTitleColor(UIColor.black, for: .normal)
        $0.setTitle("START LISTENING", for: .normal)
        $0.titleLabel?.font = UIFont.appFont(ofSize: 14, fontType: .Inter_Blod)
    }
    
    override func setupUI() {
        super.setupUI()
        self.backgroundColor = .themeBlackColor
        
        self.addSubview(infoLabel)
        self.addSubview(startButton)
        
        infoLabel.snp.makeConstraints { make in
            make.top.equalTo(23.DScale)
            make.leading.equalTo(27.DScale)
            make.trailing.equalTo(-27.DScale)
            make.height.equalTo(30.DScale)
        }
        
        startButton.snp.makeConstraints { make in
            make.top.equalTo(infoLabel.snp.bottom).offset(16)
            make.leading.equalTo(16.DScale)
            make.trailing.equalTo(-16.DScale)
            make.height.equalTo(36.DScale)
        }
        
        startButton.addTarget(self, action: #selector(clickStart(_:)), for: .touchUpInside)
    }
    
    @objc func clickStart(_ sender: UIButton)  {
        if let startAction = startAction {
            startAction()
        }
    }
    
}
