//
//  MemberListHeaderView.swift
//  Duom
//
//  Created by kuroky on 2022/9/16.
//

import UIKit
import DuomBase

class MemberListHeaderView: BaseView {
    var leaveAction: (() -> Void)?
    
    private let leaveButton = UIButton().then {
        $0.setEnlargeEdge(20)
        $0.setImage(UIImage(named: "arrow_down"), for: .normal)
    }
    
    private let titleLabel = UILabel().then {
        $0.text = "Guests"
        $0.textColor = UIColor.textWhiteColor
        $0.font = UIFont.appFont(ofSize: 20, fontType: .GT)
        $0.textAlignment = .center
    }
    
    private let countButton = DuomButton(type: .custom).then {
        $0.setImage(UIImage(named: "member_gray"), for: .normal)
        $0.setImage(UIImage(named: "member_gray"), for: .selected)
        $0.titleLabel?.font = UIFont.appFont(ofSize: 14, fontType: .Inter_Medium)
        $0.padding = 7
        $0.setTitleColor(UIColor.grayColor, for: .normal)
        $0.setEnlargeEdge(20)
    }
    
    
    override func setupUI() {
        backgroundColor = UIColor.themeBlackColor
        addSubview(leaveButton)
        addSubview(titleLabel)
        addSubview(countButton)
        leaveButton.snp.makeConstraints { make in
            make.leading.equalTo(24.DScale)
            make.top.equalTo(28.DScale)
            make.size.equalTo(CGSize(width: 8, height: 4))
        }
        
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(18.DScale)
            make.centerX.equalToSuperview()
            make.height.equalTo(24.DScale)
        }
        
        countButton.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(9.DScale)
            make.centerX.equalToSuperview()
            make.size.equalTo(CGSize(width: 25.DScale, height: 18.DScale))
        }
    }
    
    override func addActions() {
        leaveButton.addTarget(self, action: #selector(leaveAction(_:)), for: .touchUpInside)
    }
    
    @objc func leaveAction(_ sender: UIButton)  {
        if let action = leaveAction {
            action()
        }
    }
    
    func updateCount(_ count: Int) {
        self.countButton.setTitle("\(count)", for: .normal)
    }
}
