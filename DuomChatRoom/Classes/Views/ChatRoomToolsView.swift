//
//  ChatRoomToolsView.swift
//  Duom
//
//  Created by kuroky on 2022/9/14.
//

import UIKit
import DuomBase
import RxCocoa
import RxSwift

class ChatRoomToolsView: BaseView {
    var groupMuting: Bool = false
    
    private let lineView: UIView = UIView().then {
        $0.backgroundColor = .themeBlackColor65
    }
    
    private let micButton: DuomButton = DuomButton(type: .custom).then {
        $0.titleLabel?.font = UIFont.appFont(ofSize: 12, fontType: .Inter_Medium)
        $0.duomAlignment = .normal
        $0.setTitleColor(UIColor.textWhiteColor, for: .normal)
        $0.padding = 8.DScale
        $0.setTitle("Mic is on", for: .normal)
        $0.setTitle("Mic is off", for: .selected)
        $0.setImage(UIImage(named: "mic_on"), for: .normal)
        $0.setImage(UIImage(named: "live_tool_micoff"), for: .selected)
    }
    
    private let requestMicButton: DuomButton = DuomButton(type: .custom).then {
        $0.titleLabel?.font = UIFont.appFont(ofSize: 12, fontType: .Inter_Medium)
        $0.duomAlignment = .normal
        $0.setTitleColor(UIColor.textWhiteColor, for: .normal)
        $0.padding = 8.DScale
        $0.setImage(UIImage(named: "mic_on"), for: .normal)
        $0.setTitle("Request", for: .normal)
    }
    
    private let messageButton: DuomCustomBadgeButton = DuomCustomBadgeButton().then {
        $0.setEnlargeEdge(5)
        $0.setImage(UIImage(named: "tools_message"), for: .normal)
        $0.config = .init(
            size: .custom(CGSize(width: 13.DScale, height: 13.DScale)),
            corner: .value(13.DScale / 2),
            offset: .zero,
            padding: .init(hAxis: 6.DScale, vAxis: 5.DScale),
            position: .rightTop,
            font: UIFont.appFont(ofSize: 8, fontType: .Inter_Medium),
            textColor: UIColor.themeBlackColor,
            backgroundColor: UIColor.themeColor
        )
    }
    
    private let muteButton: UIButton = UIButton().then {
        $0.setEnlargeEdge(10)
        $0.setTitleColor(.white, for: .normal)
        $0.setTitleColor(UIColor(86, 86, 86), for: .selected)
        $0.setTitle("Mute all", for: .normal)
        $0.setTitle("Mute all", for: .highlighted)
        $0.setTitle("Mute all", for: .selected)
        $0.titleLabel?.font = .appFont(ofSize: 14)
    }
    
    var role: ChatRoomRole = .listener {
        didSet {
            switch role {
            case .owner, .manager:
                requestMicButton.isHidden = true
                micButton.isHidden = false
                messageButton.isHidden = false
                muteButton.isHidden = false
            case .speaker:
                requestMicButton.isHidden = true
                micButton.isHidden = false
                messageButton.isHidden = true
                muteButton.isHidden = true
            case .listener:
                requestMicButton.isHidden = false
                micButton.isHidden = true
                messageButton.isHidden = true
                muteButton.isHidden = true
            }
        }
    }
    
    override func setupUI() {
        super.setupUI()
        self.backgroundColor = .themeBlackColor
        addSubview(lineView)
        addSubview(micButton)
        addSubview(requestMicButton)
        addSubview(messageButton)
        addSubview(muteButton)
        
        lineView.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(1)
        }
        
        requestMicButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(16.DScale)
            make.bottom.equalTo(-28.DScale)
            make.width.equalTo(55.DScale)
        }
        
        micButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(16.DScale)
            make.bottom.equalTo(-28.DScale)
            make.width.equalTo(55.DScale)
        }
        
        messageButton.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 30.DScale, height: 30.DScale))
            make.trailing.equalTo(-21)
            make.top.equalTo(25.DScale)
        }
        
        muteButton.snp.makeConstraints { make in
            make.centerY.equalTo(messageButton.snp.centerY)
            make.size.equalTo(CGSize(width: 60, height: 20).DScale)
            make.leading.equalTo(24.DScale)
        }
        
        micButton.addTarget(self, action: #selector(clickSwitchMic(sender:)), for: .touchUpInside)
        requestMicButton.addTarget(self, action: #selector(requestMicAction(sender:)), for: .touchUpInside)
        messageButton.addTarget(self, action: #selector(clickMessageAction(sender:)), for: .touchUpInside)
        muteButton.addTarget(self, action: #selector(clickMuteAction(sender:)), for: .touchUpInside)
    }

    @objc func clickSwitchMic(sender: UIButton)  {
        guard !groupMuting else {
            return
        }
        sender.isSelected.toggle()
        emitRouterEvent(sender.isSelected ? ChatRoomMainViewController.Event.muteSelf : ChatRoomMainViewController.Event.unMuteSelf)
    }
    
    @objc func requestMicAction(sender: UIButton)  {
        let requestVC = ListenerRequestViewController()
        UIApplication.topViewController()?.present(requestVC, animated: true)
    }
    
    @objc func clickMessageAction(sender: UIButton)  {
        let requestVC = RequestListViewController()
        UIApplication.topViewController()?.present(requestVC, animated: true)
    }
    
    @objc func clickMuteAction(sender: UIButton) {
        sender.isSelected.toggle()
        emitRouterEvent(sender.isSelected ? ChatRoomMainViewController.Event.muteAll : ChatRoomMainViewController.Event.unMuteAll)
    }
}

extension ChatRoomToolsView {
    // 更新群组禁言状态
    func updateGroupMuteStatu(_ isMuting: Bool)  {
        self.groupMuting = isMuting
    }
    // 更新麦状态
    func updateMicStatus(_ micIsOff: Bool)  {
        micButton.isSelected = micIsOff
    }
    
    // 更新speaker状态
    func updateSpeakerStatus(_ isSpeaker: Bool)  {
        requestMicButton.isHidden = isSpeaker
        micButton.isHidden = !isSpeaker
    }
    
    // 更新消息数
    func updateMessageCount(_ count: Int)  {
        messageButton.settingBadgeValue(value: count, max: 99)
    }
}
