//
//  ChatRoomHeaderView.swift
//  Duom
//
//  Created by kuroky on 2022/9/14.
//

import Foundation
import DuomBase
import UIKit
import CloudKit

class ChatRoomHeaderView: BaseView {
    var leaveAction: (()->Void)?
    
    var miniSizeAction: (()->Void)?
    
    var modifyAction: ((String)->Void)?
    
    var role: ChatRoomRole = .listener
    
    convenience init(role: ChatRoomRole) {
        self.init(frame: .zero)
        self.role = role
        self.updateSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private let leaveIconButton: UIButton = UIButton().then {
        $0.setEnlargeEdge(.all(20))
        $0.setImage(UIImage(named: "arrow_down"), for: .normal)
    }
    
    private let roomNameTextFiled: UITextField = UITextField().then {
        $0.textColor = UIColor.textWhiteColor
        $0.font = UIFont.appFont(ofSize: 18.DScale, fontType: .GT)
        $0.textAlignment = .left
        $0.isUserInteractionEnabled = false
        $0.returnKeyType = .done
    }
    
    private let ownerNameLabel: UILabel = UILabel().then {
        $0.textColor = UIColor.themeColor
        $0.font = UIFont.appFont(ofSize: 14, fontType: .Inter_Medium)
        $0.textAlignment = .left
    }
    
    private let leaveButton: UIButton = UIButton().then {
        $0.setTitleColor(UIColor.themeRedColor, for: .normal)
        $0.titleLabel?.font = UIFont.appFont(ofSize: 13.DScale, fontType: .Inter_Medium)
    }
    
    private let modifyButton: UIButton = UIButton().then {
        $0.setEnlargeEdge(.all(15))
        $0.setImage(UIImage(named: "chatRoom_edit_title"), for: .normal)
    }
    
    
    override func setupUI() {
        self.addSubview(leaveIconButton)
        self.addSubview(roomNameTextFiled)
        self.addSubview(ownerNameLabel)
        self.addSubview(leaveButton)
        
        roomNameTextFiled.delegate = self
        
        leaveButton.addTarget(self, action: #selector(leaveAction(_:)), for: .touchUpInside)
        leaveIconButton.addTarget(self, action: #selector(miniSizeAction(_:)), for: .touchUpInside)
    }
    
    
    private func updateSubviews()  {
        self.leaveButton.setTitle((role == .owner || role == .manager) ? "End" : "Leave", for: .normal)
        
        if self.role == .owner || self.role == .manager {
            self.addSubview(modifyButton)
            
            leaveIconButton.snp.makeConstraints { make in
                make.leading.equalTo(8.DScale)
                make.top.equalTo(20)
                make.size.equalTo(CGSize(width: 20, height: 20).DScale)
            }
            
            roomNameTextFiled.snp.makeConstraints { make in
                make.top.equalTo(leaveIconButton.snp.bottom).offset(12.DScale)
                make.leading.equalTo(15)
                make.height.equalTo(22)
            }
            
            leaveButton.snp.makeConstraints { make in
                make.trailing.equalTo(-20.DScale)
                make.size.equalTo(CGSize(width: 40, height: 16).DScale)
                make.top.equalTo(20)
            }
            
            ownerNameLabel.snp.makeConstraints { make in
                make.top.equalTo(roomNameTextFiled.snp.bottom).offset(2)
                make.leading.equalTo(roomNameTextFiled.snp.leading)
                make.height.equalTo(18)
            }
            
            modifyButton.snp.makeConstraints { make in
                make.trailing.equalTo(-28.DScale)
                make.size.equalTo(CGSize(width: 30, height: 30))
                make.centerY.equalTo(roomNameTextFiled.snp.centerY)
            }
            
            modifyButton.addTarget(self, action: #selector(modifyAction(_:)), for: .touchUpInside)
        } else {
            leaveIconButton.snp.makeConstraints { make in
                make.leading.equalTo(8.DScale)
                make.top.equalTo(20)
                make.size.equalTo(CGSize(width: 20, height: 20).DScale)
            }
            
            roomNameTextFiled.snp.makeConstraints { make in
                make.top.equalTo(17)
                make.leading.equalTo(leaveIconButton.snp.trailing).offset(10)
                make.height.equalTo(22)
                make.trailing.equalTo(leaveButton.snp.leading).offset(19.DScale)
            }
            
            leaveButton.snp.makeConstraints { make in
                make.trailing.equalTo(-20.DScale)
                make.size.equalTo(CGSize(width: 40, height: 16).DScale)
                make.top.equalTo(20)
            }
            
            ownerNameLabel.snp.makeConstraints { make in
                make.top.equalTo(roomNameTextFiled.snp.bottom).offset(4)
                make.leading.equalTo(roomNameTextFiled.snp.leading)
                make.height.equalTo(18)
            }
        }
    }
    
    @objc func leaveAction(_ sender: UIButton)  {
        if let leaveAction = leaveAction {
            leaveAction()
        }
    }
    
    @objc func miniSizeAction(_ sender: UIButton) {
        miniSizeAction?()
    }
    
    @objc func modifyAction(_ sender: UIButton)  {
        self.roomNameTextFiled.isUserInteractionEnabled = true
        self.roomNameTextFiled.becomeFirstResponder()
    }
    
    func updateTitle(_ title: String) {
        self.roomNameTextFiled.text = title
    }
}

extension ChatRoomHeaderView {
    func configHeaderView(_ title: String?, topic: String?)  {
        self.roomNameTextFiled.text = title ?? ""
        self.ownerNameLabel.text = topic ?? ""
    }
}

extension ChatRoomHeaderView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.roomNameTextFiled.isUserInteractionEnabled = false
        if let modifyAction = modifyAction, let content = textField.text, !content.isEmpty {
            modifyAction(content)
        }
        return true
    }
}
