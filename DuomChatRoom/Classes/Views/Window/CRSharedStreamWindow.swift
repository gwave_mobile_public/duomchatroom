//
//  CRSharedStreamWindow.swift
//  DuomChatRoom
//
//  Created by kuroky on 2023/1/5.
//

import Foundation
import DuomBase
import RxGesture
import RxCocoa
import RxSwift
import Lottie
import DuomNetworkKit

public class CRSharedStreamWindow {
    private static var _sharedInstance: CRSharedStreamWindow?
    
    public static func shared() -> CRSharedStreamWindow {
        guard let instance = _sharedInstance else {
            _sharedInstance = CRSharedStreamWindow()
            return _sharedInstance!
        }
        return instance
    }
    
    private init() {}
    
    public var viewModel: ChatRoomViewModel?
    
    public var roomVC: ChatRoomMainViewController?
    
    public lazy var containerView: ChatRoomWindowView = ChatRoomWindowView(frame: CGRect(x: UIScreen.screenWidth - 142, y: UIScreen.screenHeight - 100 - UIScreen.bottomOffset, width: 142, height: 46))
    
    public func showChatRoomToWindow(_ viewModel: ChatRoomViewModel?, roomVC: ChatRoomMainViewController)  {
        guard let keyWindow = UIApplication.getWindow() else {
            return
        }
        self.viewModel = viewModel
        self.roomVC = roomVC
        self.containerView.viewModel = viewModel
        keyWindow.addSubview(self.containerView)
        keyWindow.bringSubviewToFront(self.containerView)
        
        self.containerView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.panAction(_:))))
        self.containerView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.tapAction(_:))))
    }
    
    @objc private func panAction(_ gesture: UIPanGestureRecognizer)  {
        guard let panView = gesture.view else { return }
        
        switch gesture.state {
        case .began, .changed:
            let translation = gesture.translation(in: panView)
            gesture.setTranslation(.zero, in: panView)
            panView.center = CGPoint(x: panView.center.x + translation.x, y: panView.center.y + translation.y)
            let location = panView.center
            if location.x > UIScreen.screenWidth / 2.f {
                containerView.updateSemanticContentAttribute(isLTR: false)
            } else {
                containerView.updateSemanticContentAttribute(isLTR: true)
            }
        case .ended, .cancelled, .failed:
            let location = panView.center
            let safeBottom = UIScreen.bottomOffset
            let safeTop = UIScreen.navigationBarHeight
            // 140 RN底部最大按钮高度
            // 23=46/2 浮球本身高度
            // 142 浮球本身宽度
            let centerY = max(min(max(location.y, safeTop + 23.f + 10.f), UIScreen.main.bounds.maxY - safeBottom - 23.f), UIScreen.statusBarHeight + 23.f)
            let centerX: CGFloat
            if location.x > UIScreen.screenWidth / 2.f {
                centerX = UIScreen.screenWidth - 142.f / 2
            } else {
                centerX = 142.f / 2
            }
            UIView.animate(withDuration: 0.3) {
                panView.center = CGPoint(x: centerX, y: centerY)
            }
        default:
            break
        }
    }
    
    @objc private func tapAction(_ sender: UITapGestureRecognizer)  {
       openCurrentRoom()
    }
    
    public func hidden() {
        containerView.isHidden = true
    }
    
    public func show() {
        containerView.isHidden = false
    }
    
    public static func destroy() {
        _sharedInstance?.containerView.removeFromSuperview()
        _sharedInstance = nil
    }
    
    public func showEndChatRoomAlert(complection: (() -> Void)? = nil) {
        containerView.showEndAlert {
            complection?()
        }
    }
    
    public func showLeaveChatRoomAlert(complection: (() -> Void)? = nil) {
        containerView.showLeaveAlert {
            complection?()
        }
    }
    
    // 需要外部调用
    public func openCurrentRoom() {
        containerView.removeFromSuperview()
        roomVC?.hiddenCurrentChainsViews(false)
    }
    
    // 开启语聊房推拉流
    public func openRoomStream() {
        viewModel?.openRoomStream()
    }
    
    // 关闭语聊房推拉流
    public func closeRoomStream() {
        viewModel?.closeRoomStream()
    }
}

public class ChatRoomWindowView: BaseView, ViewType {
    public var disposeBag = DisposeBag()
    public typealias TargetViewModel = ChatRoomViewModel
    
    private let closeButton: UIButton = UIButton().then {
        $0.setImage(UIImage(named: "chatroom_window_close"), for: .normal)
        $0.setImage(UIImage(named: "chatroom_window_close"), for: .selected)
        $0.setEnlargeEdge(.all(15))
    }

    private var nameLabel: UILabel = UILabel().then {
        $0.textColor = .white
        $0.font = .appFont(ofSize: 14)
        $0.textAlignment = .center
        $0.text = "Chat Room"
    }

    private var talkingImageView: UIImageView = UIImageView().then {
        $0.image = UIImage(named: "mic_wave")
        $0.contentMode = .scaleToFill
    }
    
    private let animationView = AnimationView(name: "speaker_wave", bundle: CRConfig.default().getCRLottieBundle).then {
        $0.contentMode = .scaleToFill
    }
    
    private(set) var isPlaying: Bool = false {
        didSet {
            talkingImageView.isHidden = isPlaying
            animationView.isHidden = !isPlaying
        }
    }
    
    public override func setupUI() {
        super.setupUI()
        backgroundColor = .themeBlackColor
        addSubview(closeButton)
        addSubview(nameLabel)
        addSubview(talkingImageView)
        addSubview(animationView)
        animationView.isHidden = true
        
        setupCorners(rect: bounds, corner: CornerConfig(topLeft: bounds.height / 2, bottomLeft: bounds.height / 2))
        
        closeButton.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalTo(-4)
            make.size.equalTo(CGSize(width: 20, height: 20))
        }
        closeButton.addTarget(self, action: #selector(closeAction(_:)), for: .touchUpInside)

        nameLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalTo(closeButton.snp.leading).offset(-10)
            make.height.equalTo(16.DScale)
        }

        talkingImageView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(12)
            make.size.equalTo(CGSize(width: 17, height: 20))
        }
        
        animationView.snp.makeConstraints { make in
            make.centerY.equalTo(talkingImageView.snp.centerY)
            make.leading.equalTo(talkingImageView.snp.leading)
            make.size.equalTo(talkingImageView.snp.size)
        }
    }
    
    public func updateSemanticContentAttribute(isLTR: Bool) {
        semanticContentAttribute = isLTR ? .forceRightToLeft : .forceLeftToRight
        let ltrCorner = CornerConfig(topRight: bounds.height / 2, bottomRight: bounds.height / 2)
        let rtlCorner = CornerConfig(topLeft: bounds.height / 2, bottomLeft: bounds.height / 2)
        setupCorners(rect: bounds, corner: isLTR ? ltrCorner : rtlCorner)
    }
    
    public func bind(viewModel: ChatRoomViewModel) {
        viewModel.state.volumeSpeakerPublish
            .subscribe(onNext: { [weak self] _ in
                guard let self = self else { return }
                if self.isPlaying {
                    return
                }
                self.isPlaying = true
                self.animationView.play { [weak self] _ in
                    self?.isPlaying = false
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.state.groupIsDismiss
            .subscribe(onNext: { [weak self] in
                self?.removeFromSuperview()
            })
            .disposed(by: disposeBag)
    }

    @objc private func closeAction(_ sender: UITapGestureRecognizer)  {
        if viewModel?.role == .owner || viewModel?.role == .manager {
            showEndAlert()
        } else {
            showLeaveAlert()
        }
    }
    
    // 主播主动结束
    public func showEndAlert(complection: (() -> Void)? = nil) {
        let alert = ChatRoomAlert(title: CRConfig.default().endRoomTitle,
                                  message: CRConfig.default().endRoomAuthorMessage,
                                  cancelTitle: "CANCEL",
                                  doneTitle: "END")
        alert.leaveAction = { [weak self] _ in
            self?.viewModel?.action.onNext(.endRoom)
            self?.removeFromSuperview()
            complection?()
        }
        UIApplication.topViewController()?.present(alert, animated: true)
    }
    
    public func showLeaveAlert(complection: (() -> Void)? = nil) {
        self.viewModel?.action.onNext(.leaveRoom)
        self.removeFromSuperview()
        complection?()
    }
    
}
